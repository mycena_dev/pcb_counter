from tinydb import TinyDB
import tinydb_encrypted_jsonstorage as tae


class DBAgent:
    __KEY = "hl8QjR3ekV"
    __PATH = "./system.data"
    __db = TinyDB(encryption_key=__KEY, path=__PATH, storage=tae.EncryptedJSONStorage)

    config_table = __db.table('config')
    account_table = __db.table('account')
