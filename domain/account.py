class PolicySetting:
    config = {
        "can_edit_field_name": False,
        "can_edit_ng_threshold": False,
        "can_edit_export_path": False,
        "can_edit_upload_url": False,
        "can_upload_csv": False,
        "can_edit_account": False
    }


class Account:
    def __init__(self, username: str = "", password: str = "", job_number: str = "",
                 can_edit_field_name: bool = False,
                 can_edit_ng_threshold: bool = False,
                 can_edit_export_path: bool = False,
                 can_edit_upload_url: bool = False,
                 can_upload_csv: bool = False,
                 can_edit_account: bool = False,
                 db_document=None):

        if db_document is not None:
            self.username = db_document['username']
            self.job_number = db_document['job_number']
            self.can_edit_account = db_document['can_edit_account']
            self.can_upload_csv = db_document['can_upload_csv']
            self.can_edit_upload_url = db_document['can_edit_upload_url']
            self.can_edit_export_path = db_document['can_edit_export_path']
            self.can_edit_ng_threshold = db_document['can_edit_ng_threshold']
            self.can_edit_field_name = db_document['can_edit_field_name']
            self.password = db_document['password']
        else:
            self.username = username
            self.password = password
            self.job_number = job_number
            self.can_edit_account = can_edit_account
            self.can_upload_csv = can_upload_csv
            self.can_edit_upload_url = can_edit_upload_url
            self.can_edit_export_path = can_edit_export_path
            self.can_edit_ng_threshold = can_edit_ng_threshold
            self.can_edit_field_name = can_edit_field_name
