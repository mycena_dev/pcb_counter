from pypylon import pylon
import cv2

camera = pylon.InstantCamera(pylon.TlFactory.GetInstance().CreateFirstDevice())
camera.Open()

# demonstrate some feature access
new_width = camera.Width.GetValue() - camera.Width.GetInc()
if new_width >= camera.Width.GetMin():
    camera.Width.SetValue(new_width)

numberOfImagesToGrab = 100
camera.StartGrabbingMax(numberOfImagesToGrab)
while camera.IsGrabbing():
    grabResult = camera.RetrieveResult(5000, pylon.TimeoutHandling_ThrowException)
    if grabResult.GrabSucceeded():
        # Access the image data.
        # print("SizeX: ", grabResult.Width)
        # print("SizeY: ", grabResult.Height)

        img = grabResult.Array
        print(img)
        cv2.imshow("Gray value of first pixel: ", cv2.resize(img,(500,500)))
        cv2.waitKey(1)

    grabResult.Release()
camera.Close()