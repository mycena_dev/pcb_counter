from _init_paths import *
from Controller.ImageSystemController import ImageSystemController
from Controller.PcbCounterController import PcbCounterController
from Controller.DetectNGController import DetectNGController

import sys




img_path = '{}'.format(sys.argv[1])
config_path='./config/pcb.ini'
state="Turn on"

imageSystemController = ImageSystemController()
detectNGController = DetectNGController()
detectNGController.setConfiguration(baselineThreshold=0.1)
pcbCounterController = PcbCounterController()
pcbCounterController.setConfigPath(config_path)
pcbCounterController.setGapSwitch(state)



imageSystemController.setImageByPath(img_path)
print(detectNGController.detectNG(imageSystemController.getImage()))
result = pcbCounterController.run(imageSystemController.getImage())
print(result)


