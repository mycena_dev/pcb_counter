import os.path as osp
import sys

def add_path(path):
    if path not in sys.path:
        sys.path.insert(0, path)

# Add lib to PYTHONPATH
this_dir = osp.dirname(__file__)
lib_path = osp.join(this_dir, '../marcus_utils') #library路徑
add_path(lib_path)

lib_path = osp.join(this_dir, '../marcus_utils/third_party/yolo_utils') #library路徑
add_path(lib_path)


