from _init_paths import *
from Controller.ImageSystemController import ImageSystemController
from Controller.PcbCounterController import PcbCounterController
from Controller.DetectNGController import DetectNGController
import sys
import glob

config_path='./config/pcb.ini'
state="Turn on"

imageSystemController = ImageSystemController()
detectNGController = DetectNGController()
detectNGController.setConfiguration(baselineThreshold=0.1)
pcbCounterController = PcbCounterController()
pcbCounterController.setConfigPath(config_path)
pcbCounterController.setGapSwitch(state)
data_path = "OPC_dataset/data0922/*/*.jpg"
print(glob.glob(data_path))
for img_path in glob.glob(data_path):
    print(img_path)
    print( "=======================================")
    
    imageSystemController.setImageByPath(img_path)
    print(detectNGController.detectNG(imageSystemController.getImage()))
    result = pcbCounterController.run(imageSystemController.getImage())
    print(result)