
from Service.PreprocessImgService import PreprocessImgService
from Service.calculateService import PcbCalculateService
class CounterService:
    def __init__(self):
        self.preprocessService  =PreprocessImgService()
        self.calculateService = PcbCalculateService()    

    def setConfiguration(self,counterConfig):
        self.height=int(170)
        self.ratio=float(0.8)                
    def count(self, inputImage, label,state):
        preprocessImg, self.preprocessImgList = self.preprocessService.run(inputImage)
        countResult, self.countImgList, label = self.calculateService.run(preprocessImg,170,0.8,state)
        return countResult, label
    def getSavePreprocessImgList(self):
        return self.preprocessImgList
    def getSaveCalculateImgList(self):
        return self.countImgList