import configparser


class ConfigService:
    def __init__(self):
        pass

    def setConfigPath(self, configpath):
        config = configparser.ConfigParser()
        config.read(configpath)
        self.config = config
        # print(configpath)

    def getYoloConfig(self):
        checkpointpath = self._getYoloCheckpointPath()
        return checkpointpath

    def _getYoloCheckpointPath(self):
        return self.config["yolosql"]["checkpoint"]

    def getCounterConfig(self):
        height = self._getFindPeakheight()
        ratio = self._getFindPeakRatio()
        countConfigSet = [height, ratio]
        return countConfigSet

    def _getFindPeakheight(self):
        return self.config["peaksql"]["height"]

    def _getFindPeakRatio(self):
        return self.config["peaksql"]["ratio"]

    def getPanelConfig(self):
        checkpointpath = self._getPanelCheckpointPath()
        return checkpointpath


    def _getPanelCheckpointPath(self):
        return self.config["panelsql"]["checkpoint"]