from DAO.Data.Image import Image

class ImageLoaderService:
    def __init__(self):
        self._image = Image()

    def setImageByPath(self, imgPath):
        self._image.setImageByPath(imgPath)

    def getImage(self):
        return self._image.getImage()