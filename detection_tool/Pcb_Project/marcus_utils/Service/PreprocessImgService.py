import cv2
import numpy as np
import time
from DAO.Process.preprocess_utils.bgr2gray import Bgr2gray_Class
from DAO.Process.preprocess_utils.auto_bright import Auto_bright_contrast_Class
from DAO.Process.preprocess_utils.filter2d import Filter2d_Class
from DAO.Process.preprocess_utils.dilate import Dilate_Class
from DAO.Process.preprocess_utils.svd import SVD_Class 
from DAO.Process.preprocess_utils.find_center import find_center_Class
from DAO.Process.preprocess_utils.rotio import RatioLabel_Class
from DAO.Process.preprocess_utils.threshold import Threshold_Class
from DAO.Process.preprocess_utils.testmode import TestMode_Class
from DAO.Process.preprocess_utils.gapdetect import gapDetect_Class
from DAO.Process.preprocess_utils.crop import Crop_Class

class PreprocessImgService:
	def __init__(self):
		self.find_center=find_center_Class()
		self.bgr2gray = Bgr2gray_Class()
		self.auto_bright = Auto_bright_contrast_Class()
		self.filter2d = Filter2d_Class()
		self.dilate = Dilate_Class()
		self.ratio = RatioLabel_Class()
		self.svd = SVD_Class()
		self.threshold= Threshold_Class()
		self.testmode = TestMode_Class()
		self.gapdetect = gapDetect_Class()
		self.crop = Crop_Class()   
		self.w, self.h = 4000,4000
		self.saveDirPath = "./processData"
		self.ratio.setRatio(0.475, 0.525)
		self.imgList=[]
	def  run(self,inputImage):
        #setRatio
		startRatio, endRatio= self.ratio.getRatio()

        #find PCB center 
		self.find_center.setImg(inputImage, startRatio, endRatio)
		self.center_line, self.upline, self.downline= self.find_center.run()	
		self.filter2d.set_proportion_h(self.center_line)
        #first preprocess
		grayImg =self.bgr2gray.run(inputImage)
		brightImg =self.auto_bright.run(grayImg)
		filterImg = self.filter2d.run(brightImg)
		dilateImg = self.dilate.run(filterImg)
		th2Img = self.threshold.run(brightImg)

        #resize img
		img = cv2.resize(dilateImg ,(self.w,self.h))
		oriImg =cv2.resize(inputImage ,(self.w,self.h))
		th2 = cv2.resize(th2Img ,(self.w,self.h))

        #crop ROI img
		self.crop.setInputImg(oriImg=oriImg, img=img, th2=th2)
		self.crop.setCropCoordinate(coordinate=[startRatio, endRatio])
		cropImg = self.crop.getCropImg()
		th3 = self.crop.getCropTh2()
		oriImg = self.crop.getCropOriImg()

        #second preprocess
		start = time.time()
		svdImg =self.svd.run(cropImg)
		svdImg = self._insertLossPixel(svdImg, self.center_line)
		print("svd cost time: ",time.time()-start)
		#get first mode
		firstMode, low_standard, endPeak = self.findPeakFirst(svdImg)

		#find Gap and create GapDict
		GapDict, gapStartIndex, gapEndIndex, GapStandard = self.generateGapDict(th2, firstMode, endPeak)

		#visualizationGapImg
		visImg = self._visualizationGap(svdImg, th3, gapStartIndex, gapEndIndex, GapDict)

		#setImg
		self.imgList.append([grayImg,"gray"])
		self.imgList.append([brightImg,"bright"])

		return [svdImg, th3, gapStartIndex, gapEndIndex, GapStandard, firstMode, GapDict, low_standard , oriImg, visImg], self.imgList
       
	def findPeakFirst(self,svdImg):
		line=svdImg[:,int(0.5*svdImg.shape[1])]
		firstMode, low_standard, firstPeak, endPeak = self.testmode(line)
		return firstMode, low_standard, endPeak
	def _getGapStandard(self,mode):
		if mode > 30 : 
			GapStandard = 5
		else:
			GapStandard = 3
		return GapStandard			
	def generateGapDict(self, th2, firstMode, endPeak):
		startRatio, endRatio= self.ratio.getRatio()
		intStartPoint,intEndPoint=int(self.w*startRatio),int(self.w*endRatio)
		gapStartIndex, gapEndIndex = 0, endPeak         #設定 檢測gap 的起始點與終點
		GapStandard = self._getGapStandard(firstMode)   #設定 Gap標準值，當大於標準即為Gap
		th4 = th2[:,intStartPoint-50:intEndPoint+50]  	#將th2加寬，作為GapDetect輸入
		GapDict = self.gapdetect(th4 ,gapStartIndex ,gapEndIndex ,GapStandard)   
		return GapDict, gapStartIndex, gapEndIndex, GapStandard
	def _visualizationGap(self, svd, th3, startLine, endLine, GapDict):
		svd = svd.astype('float32')
		visImg = np.hstack((th3,svd))		
		visImg = cv2.cvtColor(visImg,cv2.COLOR_GRAY2BGR)
		vis_h,vis_w,_=visImg.shape
		cv2.line(visImg, (0,startLine), (vis_w,startLine), (0,255,0), 3)
		cv2.line(visImg, (0,endLine), (vis_w,endLine), (0,255,0), 3)	

		for gap in GapDict:
			bound = GapDict["{}".format(gap)]
			y1,y2 = bound[0]+startLine, bound[1]+startLine
			cv2.line(visImg, (0,y1),(vis_w,y1) ,(0,0,255), 2)
			cv2.line(visImg, (0,y2),(vis_w,y2) ,(0,0,255), 2)	
			
		cv2.line(visImg, (0,int(self.center_line*vis_h)),(vis_w,int(self.center_line*vis_h)) ,(0,100,0), 3)	
		return visImg
	def _insertLossPixel(self,img, centerLine):
		img = img.astype('float32')
		# img = cv2.cvtColor(img,cv2.COLOR_GRAY2BGR)
		
		h, w= img.shape
		centerLine= int(centerLine * h)
		newImgShape = (h, w)	
		newImg = np.full(newImgShape, 255).astype(np.float32)		
		newImg[:centerLine,:] = img[:centerLine,:]
		newImg[centerLine+5:,:] = img[centerLine:-5,:]
		newImg[:,:5] = 0
		newImg[:,-5:] = 0	
		return newImg

