from DAO.Process.calculate_utils.findpeak import findPeak_Class
from DAO.Process.calculate_utils.countplate import countPlate_Class
from DAO.Process.calculate_utils.fixedline import fixedLine_Class

import numpy as np
import cv2
import random
class PcbCalculateService:
	def __init__(self):
		self.findPeak = findPeak_Class()
		self.fixedLine = fixedLine_Class()
		self.test_ration = (np.arange(0,1,0.1))[4:]
		
	def run(self, PreprocessImgList,height,ratio,state):
		inputImage, th2, gapStartIndex, gapEndIndex, GapStandard, firstMode, GapDict, low_standard, oriImg, visImg= PreprocessImgList
		h,w = inputImage.shape
		countList = []
		labelList = []
		for (i,r) in enumerate(self.test_ration):
			# state = "Turn off"
			ori_line =inputImage[:,int(r*w)]
			self.findPeak.setLine(ori_line)
			sum_list,n_peaks1,leastMode = self.findPeak.run(height,ratio,firstMode, low_standard)
			newGapStartIndex = n_peaks1[0]
			if state == "Turn on":
				th2Line=th2[:,int(r*w)]
				self.fixedLine.setFixedLine(th2Line, newGapStartIndex, gapEndIndex, firstMode, GapDict, GapStandard)  # line以0為起點 去檢測是否有縫隙
				self.fixedLine.findGap()
				gapIndex = self.fixedLine._chooseCorrectGap()
				line=ori_line
			elif state =="Turn off":
				gapIndex=[]
				line=ori_line
			gapList = self.findPeak.visualGap(gapIndex)
			coverList, result, newPeakList, errorList, label = self.findPeak.getPCBNumber(sum_list, n_peaks1, gapList, leastMode)
			countList.append(result)
			if label  == "normal":
				 labelList.append(label)
#===================================visualization===================================#
			if i==3:
				outputImg= self.findPeak.drawPeak(newPeakList, coverList, gapList, errorList, oriImg, visImg, inputImage)	
		if len(labelList)<len(self.test_ration):
			label = "ambiguous"
		return np.argmax(np.bincount(countList)), [outputImg,"output"], label
