import torch
from third_party.yolo_utils.utils.general import  non_max_suppression, scale_coords, xyxy2xywh
from third_party.yolo_utils.models.experimental import attempt_load
from third_party.yolo_utils.utils.datasets import letterbox

import numpy as np

class YoloService:
	def __init__(self):
		pass
	
	def setConfiguration(self, checkpointPath, augment=False, confThres=0.25, iouThres=0.45, classes=None, 
						agnosticNMS=False, gpuMode=False):
		#set parameters
		self.augment = augment
		self.conf_thres = confThres
		self.iou_thres = iouThres
		self.classes = classes
		self.agnostic_nms = agnosticNMS

		# set model
		if gpuMode != True:
			self.device = torch.device("cpu")
		else:
			self.device = torch.device("cuda")
		self.checkpointPath = checkpointPath
		self.model = attempt_load(checkpointPath, map_location=self.device)
		self.names = self.model.module.names if hasattr(self.model, 'module') else self.model.names

	def detectObject(self, inputImage):
		preprocessingImage = self._preprocessing(inputImage)
		pred = self.model(preprocessingImage, self.augment)[0]
		pred = non_max_suppression(pred,self.conf_thres,self.iou_thres,self.classes,self.agnostic_nms)
		[cropImage, label] = self._obtainBoundingBox(pred, preprocessingImage, inputImage)

		return [cropImage, label]

	def _preprocessing(self, inputImage):
		img =  letterbox(inputImage)[0]
		img = img[:, :, ::-1].transpose(2, 0, 1)
		img = np.ascontiguousarray(img)
		img = torch.from_numpy(img).to(self.device)
		img=img.float()
		img /=255.0
		if img.ndimension() == 3:
			img=img.unsqueeze(0)
		return img

	def _obtainBoundingBox(self, pred, preprocessingImage, inputImage):
		for _,det in enumerate(pred):
			gn = torch.tensor(inputImage.shape)[[1, 0, 1, 0]]  # normalization gain whwh
			if len(det):
				# Rescale boxes from img_size to im0 size
				det[:, :4] = scale_coords(preprocessingImage.shape[2:], det[:, :4], inputImage.shape).round()
				for *xyxy, conf, cls in reversed(det):
					label = f'{self.names[int(cls)]}'
					x,y,w,h = (xyxy2xywh(torch.tensor(xyxy).view(1, 4)) / gn).view(-1).tolist() 
					H,W,_=inputImage.shape
					x1=int(W*(x-w/2))
					y1=int(H*(y-h/2)) 
					x2=int(W*(x+w/2))
					y2=int(H*(y+h/2))
					crop_img=inputImage[y1:y2,x1:x2]
			else:
				crop_img=0; label="panel"
		return [crop_img, label]

	def detectPanel(self,cropImg):
		preprocessingImage = self._preprocessing(cropImg)
		pred = self.model(preprocessingImage, self.augment)[0]
		pred = non_max_suppression(pred,self.conf_thres,self.iou_thres,self.classes,self.agnostic_nms)
		[cropImage, label] = self._obtainBoundingBox(pred, preprocessingImage, cropImg)
		return label