from Service.DetectFailService import DetectFailService

class DetectNGController:
    def __init__(self):
        self.detectFailService = DetectFailService()

    def setConfiguration(self, baselineThreshold):
        self.detectFailService.setConfiguration(baselineThreshold)

    def detectNG(self,inputImage):
        state = self.detectFailService.detect(inputImage=inputImage)
        return {"state": state}
