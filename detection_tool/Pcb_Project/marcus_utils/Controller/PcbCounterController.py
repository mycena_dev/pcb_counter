from Service.YoloService import YoloService
from Service.panelDetectService import PanelDetectService
from Service.CounterService import CounterService
from Service.ConfigService import ConfigService
from Service.GapDeleteService import GapDeleteService
from Service.PreprocessImgService import PreprocessImgService
from Service.calculateService import PcbCalculateService
from Service.saveImgService import SaveImgService
import os
import cv2
import time

class PcbCounterController:
    def __init__(self):
        self.configService= ConfigService()
        self.yoloService = YoloService()
        self.panelService = PanelDetectService()
        self.counterService = CounterService()
        self.gapdeleteService = GapDeleteService()
        self.preprocessService  =PreprocessImgService()
        self.calculateService = PcbCalculateService()
        self.saveImgService = SaveImgService()

    def setConfigPath(self,configpath):
        self.configService.setConfigPath(configpath)
        self._setYoloConfig(self.configService.getYoloConfig())
        self._setCounterConfig(self.configService.getCounterConfig())
        self._setPanelConfig(self.configService.getPanelConfig())
    def _setYoloConfig(self,checkpointPath):
        self.yoloService.setConfiguration(checkpointPath=checkpointPath)

    def _setCounterConfig(self,config):
        self.counterService.setConfiguration(counterConfig=config)
    
    def _setPanelConfig(self,checkpointPath):
        self.panelService.setConfiguration(checkpointPath=checkpointPath)

    def setGapSwitch(self,state):
        self.gapdeleteService.setGapSwitch(state)

    def run(self, inputImage, imgSaveDirPath=""):
        start = time.time()
        [cropImage, label] = self.yoloService.detectObject(inputImage)
        if label == "panel":
            print("first detect find panel")
            count = 0 ; label= "panel"
        else:
            panelImg, leastLabel = self.panelService.detectPanel(cropImage)
            print("panelDetect cost time: ",time.time()-start)
            if leastLabel=="panel":
                if imgSaveDirPath:
                    if not os.path.exists(imgSaveDirPath):
                            os.makedirs(imgSaveDirPath)
                    cv2.imwrite(imgSaveDirPath + "/panel.png",panelImg)
                count = 0 ; label= "panel"
                print("second detect find panel")
            else:
                count, label = self.counterService.count(cropImage, label,self.gapdeleteService.getGapSwitch())
                if imgSaveDirPath:
                    if not os.path.exists(imgSaveDirPath):
                        print("saveDirPath is not exists!!")
                        os.makedirs(imgSaveDirPath)
                    preprocessImgList = self.counterService.getSavePreprocessImgList()
                    countImgList =self.counterService.getSaveCalculateImgList()
                    self.saveImgService.savePreprocessImg(imgSaveDirPath,preprocessImgList)
                    self.saveImgService.saveCountImg(imgSaveDirPath,countImgList)    
        end = time.time()             
        resultDict={'usedTime(s)': end-start, 'result(pcs)': count, 'label': label}                                      
        return resultDict

