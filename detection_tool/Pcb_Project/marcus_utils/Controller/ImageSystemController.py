from Service.ImageLoaderService import ImageLoaderService

class ImageSystemController:
    def __init__(self):
        self._imageLoaderService = ImageLoaderService()

    def setImageByPath(self, imgPath):
        self._imageLoaderService.setImageByPath(imgPath)

    def getImage(self):
        return self._imageLoaderService.getImage()