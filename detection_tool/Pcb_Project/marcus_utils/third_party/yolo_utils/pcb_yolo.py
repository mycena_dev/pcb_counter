
import torch
from third_party.yolo_utils.models.experimental import attempt_load
from third_party.yolo_utils.utils.general import  non_max_suppression, scale_coords, xyxy2xywh


class PCB_yolo_Class:
	def __init__(self,weights):
		self.augment = False
		self.conf_thres = 0.25
		self.iou_thres = 0.45
		self.classes = None
		self.agnostic_nms = False
		self.device = torch.device("cpu")
		self.weights = weights
		self.build_model()
		
	def setWeights(self,weights):
		self.weights= weights

	def build_model(self):
		self.model = attempt_load(self.weights, map_location=self.device)	
		self.names = self.model.module.names if hasattr(self.model, 'module') else self.model.names

	def set_detect_Object(self,img,im0):
		self.img= img 
		self.im0= im0

	def run(self):
		pred = self.model(self.img, self.augment)[0]
		pred = non_max_suppression(pred,self.conf_thres,self.iou_thres,self.classes,self.agnostic_nms)
		for _,det in enumerate(pred):
			gn = torch.tensor(self.im0.shape)[[1, 0, 1, 0]]  # normalization gain whwh
			if len(det):
                # Rescale boxes from img_size to im0 size
				det[:, :4] = scale_coords(self.img.shape[2:], det[:, :4], self.im0.shape).round()
				for *xyxy, conf, cls in reversed(det):
					self.label = f'{self.names[int(cls)]}'
					x,y,w,h = (xyxy2xywh(torch.tensor(xyxy).view(1, 4)) / gn).view(-1).tolist() 
					H,W,_=self.im0.shape
					x1=int(W*(x-w/2))
					y1=int(H*(y-h/2)) 
					x2=int(W*(x+w/2))
					y2=int(H*(y+h/2))
					self.crop_img=self.im0[y1:y2,x1:x2] 

	def get_result(self):
		return self.crop_img, self.label 
