import cv2
import numpy as np
from scipy.signal import find_peaks
from matplotlib import pyplot as plt



def countLine(line):
	last = 255
	count_inner = 0
	ind_black = np.arange(len(line)-1) 
	ind_black = ind_black[(np.diff(line[:,0],n=1) == -255)]

	ind_white = np.arange(len(line)-1) 
	ind_white = ind_white[(np.diff(line[:,0],n=1) == 255)]
	ListNum = []
	for i in range(len(ind_black)-1):
		ListNum.append(ind_black[i+1] - ind_black[i])
	return ListNum

def checkMinValue(line):
	min_val = np.min(line)
	if min_val<0:
		line = np.array(line)
		new_array = line-min_val
		new_line = new_array.tolist()
	else:
		new_line = line
	return new_line
def avergy_moving(line,mode,stride):
	new_line=[]
	line = np.square(line)
	for i in range(len(line)-mode+1):
		avergy_value =np.mean(line[i:i+mode]) 
		new_line.append(np.sqrt(avergy_value))
	return new_line
def findPeak(line):
	line = 255 - line
	line = (line-line[0])**2/np.std(line)
	line = checkMinValue(line)
	line =avergy_moving(line,5,1)
	break_line = int(len(line)/5)
	peaks1, _ = find_peaks(line[0:break_line], height=150, distance=10)
	peaks2, _ = find_peaks(line[break_line:], height=300, distance=10)
	diff_line = 255*np.ones(len(line))
	diff_line[peaks1] = 0
	diff_line[peaks2+break_line] = 0
	ret2,line2 = cv2.threshold(diff_line,100,255,cv2.THRESH_BINARY)
	mode_count = np.array(countLine(line2[break_line:]))
	sum_mode=np.argmax(np.bincount(mode_count))
	new_dis= int(sum_mode*0.75)
	new_peaks, _ = find_peaks(line, height=170, distance=new_dis)
	diff_line = 255*np.ones(len(line))
	diff_line[new_peaks] = 0

	ret2,line2 = cv2.threshold(diff_line,100,255,cv2.THRESH_BINARY)
	sum_count = np.array(countLine(line2))
	mode_count = np.array(countLine(line2[break_line:]))
	sum_mode=np.argmax(np.bincount(mode_count))
	sum_list=sum_count/sum_mode

	return sum_list

def countPlateSmall(sum_list):
	count = 0
	temp_df = 0
	for (index,df) in enumerate(sum_list):
		#print("index", index, "=>", df)
		if index == 0:
			if ((df < 3) & (df > 0.75)):	
				count = count + 1
			else:
				temp_df = df
			continue
		if ((df < 1.25) & (df > 0.75)):
			count = count + 1
			if temp_df > 0.5:
				count=count + round(temp_df)
			temp_df = 0
			continue
		temp_df = temp_df + df
		if temp_df-int(temp_df) <= 0.3:
			count = count + int(temp_df)
			temp_df = 0
		elif temp_df-int(temp_df) >= 0.7:
			count = count + int(temp_df) + 1
			temp_df = 0
	count = count + round(temp_df)
	return count

def countPlateBig(new_sum_list):
	sumPart1 = new_sum_list[0:6]
	sumPart2 = new_sum_list[6:]
	count1 = 0
	count2 = 0
	for df1 in sumPart1:
		if ((df1 < 1.7) & (df1 > 0.75)):
			count1=count1+1
		else:
			count1=count1+round(df1)
	for df2 in sumPart2:
		count2 = count2+round(df2)
	count = count1 + count2
	return count
