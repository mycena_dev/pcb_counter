import cv2
import numpy as np
import math

###########轉灰階
def bgr2gray(img):
	return cv2.cvtColor(img,cv2.COLOR_BGR2GRAY)




def convertScale(img, alpha, beta):
    new_img = img * alpha + beta
    new_img[new_img < 0] = 0
    new_img[new_img > 255] = 255
    return new_img.astype(np.uint8)

##########自動調整對比
def automatic_brightness_and_contrast(image, clip_hist_percent=25):

    #gray = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
    gray = image
    # Calculate grayscale histogram
    hist = cv2.calcHist([gray],[0],None,[256],[0,256])
    hist_size = len(hist)
    # Calculate cumulative distribution from the histogram
    accumulator = []
    accumulator.append(float(hist[0]))
    for index in range(1, hist_size):
        accumulator.append(accumulator[index -1] + float(hist[index]))
    # Locate points to clip
    maximum = accumulator[-1]
    clip_hist_percent *= (maximum/100.0)
    clip_hist_percent /= 2.0
    # Locate left cut
    minimum_gray = 0
    while accumulator[minimum_gray] < clip_hist_percent:
        minimum_gray += 1
    # Locate right cut
    maximum_gray = hist_size -1
    while accumulator[maximum_gray] >= (maximum - clip_hist_percent):
        maximum_gray -= 1
    # Calculate alpha and beta values
    alpha = 255 / (maximum_gray - minimum_gray)
    beta = -minimum_gray * alpha
    auto_result = convertScale(image, alpha=alpha, beta=beta)

    return auto_result

###########找目標中心線
def find_center(img):
	h,w=img.shape 
	i1,j1=0,0
	i2,j2=h-1,w-1
	y1,y2=0,0
	for k in range(h):
		point = img[i1+k][j1+k]
		if point !=0:
			y1=i1+k
			break
	for l in range(h):
		point = img[i2-l][j2-l]
		if point !=0:
			y2=i2-l
			break
	# print(y1/h,y2/h)
	center = (int((y2-y1)/2)+y1)/h
	center = math.floor(center*100)*0.01

	return center
#######影像分割
def grabcut_img(ori_img):
	ori_img = cv2.cvtColor(ori_img, cv2.COLOR_GRAY2RGB)
	h,w,_= ori_img.shape
	n_h = int(h/10)
	n_w = int(w/10)
	img=cv2.resize(ori_img,(n_w,n_h))
	mask = np.zeros(img.shape[:2],np.uint8)
	bgdModel = np.zeros((1,65),np.float64)
	fgdModel = np.zeros((1,65),np.float64)
	rect = (1,1,n_w-1,n_h-1)
	cv2.grabCut(img,mask,rect,bgdModel,fgdModel,1,cv2.GC_INIT_WITH_RECT)
	mask2 = np.where((mask==2)|(mask==0),0,1).astype('uint8')
	mask2 = mask2*255
	mask2 = cv2.resize(mask2,(w,h))
	center_line = find_center(mask2)
	return center_line
################拉普拉斯運算
def filter2d(img):

	kernel=np.array((
		[0,1,0],
		[1,-4,1],
		[0,1,0]),dtype="float32")
	kernel=np.array((
		[1,1,1],
		[0,0,0],
		[-1,-1,-1]),dtype="float32")
	img_u = cv2.filter2D(img,-1,kernel)
	kernel=np.array((
		[-1,-1,-1],
		[0,0,0],
		[1,1,1]),dtype="float32")
	img_d = cv2.filter2D(img,-1,kernel)
	kernel=np.array((
		[1,0,-1],
		[1,0,-1],
		[1,0,-1]),dtype="float32")
	img_l = cv2.filter2D(img,-1,kernel)
	kernel=np.array((
		[-1,0,1],
		[-1,0,1],
		[-1,0,1]),dtype="float32")
	img_r = cv2.filter2D(img,-1,kernel)
	h,w = img.shape
	img_f = np.zeros((h,w))
	img_f[0:int(1*h/3),:] = img_d[0:int(1*h/3),:]
	img_f[int(1*h/3):,:] = img_u[int(1*h/3):,:]
	img_f = img_f.astype(np.uint8)

	return img_f
###########膨脹
def dilate(img): 
	kernel = np.ones((1,5),np.uint8)
	img = cv2.dilate(img,kernel,iterations = 1)

	return img
###########反白
def bitwise_not(img): 
	return cv2.bitwise_not(img)
###########SVD分解
def SVD(img):
	u,s,v = np.linalg.svd(img)
	img_out = 0
	a = s/s[0]
	kk = np.ones(len(a))
	ind = a>0.1
	s_iter = int(sum(kk[ind]))
	for ii in range(s_iter):
		img_out = img_out + s[ii]*np.outer(u[:,ii],v[ii,:])
	return img_out

