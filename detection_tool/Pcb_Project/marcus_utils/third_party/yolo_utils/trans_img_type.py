import torch
from third_party.yolo_utils.utils.datasets import letterbox
import numpy as np
class LoadImage_Class:
	def __init__(self):
		self.device = torch.device("cpu")
	def setImg(self,img0):
		self.img0 =img0
	def run(self):
		img =  letterbox(self.img0)[0]
		img = img[:, :, ::-1].transpose(2, 0, 1)
		img = np.ascontiguousarray(img)
		img = torch.from_numpy(img).to(self.device)
		img=img.float()
		img /=255.0
		if img.ndimension() == 3:
			img=img.unsqueeze(0)
		return img, self.img0
