class countPlate_Class:

	def __init__(self,sum_list):
		self.sum_list = sum_list
		self.baseline= 160
	def countPlateSmall(self,sum_list):
		count = 0
		temp_df = 0
		for (index,df) in enumerate(sum_list):
		#print("index", index, "=>", df)
			if index == 0:
				if ((df < 1.65) & (df > 0.75)):	
					count = count + 1
				else:
					temp_df = df
				continue
			if ((df < 1.25) & (df > 0.75)):
				count = count + 1
				if temp_df > 0.5:
					count=count + round(temp_df)
				temp_df = 0
				continue
			temp_df = temp_df + df
			if temp_df-int(temp_df) <= 0.3:
				count = count + int(temp_df)
				temp_df = 0
			elif temp_df-int(temp_df) >= 0.7:
				count = count + int(temp_df) + 1
				temp_df = 0
		count = count + round(temp_df)
		return count
	def countPlateBig(self,sum_list):
		sumPart1 = sum_list[0:6]
		sumPart2 = sum_list[6:]
		count1 = 0
		count2 = 0
		for df1 in sumPart1:
			if ((df1 < 1.7) & (df1 > 0.75)):
				count1=count1+1
			else:
				count1=count1+round(df1)
		for df2 in sumPart2:
			count2 = count2+round(df2)
		count = count1 + count2
		return count

	def select_countplate(self):
		if len(self.sum_list)>self.baseline:
			self.countPlate = self.countPlateBig(self.sum_list)

		else:
			self.countPlate = self.countPlateSmall(self.sum_list)
			

	def run(self):
		sheet_count = self.countPlate
		return sheet_count

