import cv2
import matplotlib.pyplot as plt
import numpy as np
from scipy.signal import find_peaks


class findPeak_Class:


	def __init__(self):
		self.mode = 5
		self.stride = 1
	def setLine(self,line):
		self.line =line	
	def countLine(self,line):
		last = 255
		count_inner = 0
		ind_black = np.arange(len(line)-1) 
		ind_black = ind_black[(np.diff(line[:,0],n=1) == -255)]

		ind_white = np.arange(len(line)-1) 
		ind_white = ind_white[(np.diff(line[:,0],n=1) == 255)]
		ListNum = []
		for i in range(len(ind_black)-1):
			ListNum.append(ind_black[i+1] - ind_black[i])
		return ListNum

	def checkMinValue(self,line):
		min_val = np.min(line)
		if min_val<0:
			line = np.array(line)
			new_array = line-min_val
			new_line = new_array.tolist()
		else:
			new_line = line
		return new_line

	def avergy_moving(self, line, mode, stride):
		new_line = []
		line = np.square(line)
		for i in range(len(line) - mode + 1):
			avergy_value = np.mean(line[i:i + mode])
			new_line.append(np.sqrt(avergy_value))
		return new_line

	def selectMode(self, mode_count):
		num_bins =2
		bins = int((np.max(mode_count) - np.min(mode_count)) / num_bins)  
		plt.close()
			#　將mode_count分成 bins段　

		if bins!=0:
			mode_hist = plt.hist(mode_count, bins=bins) 
				# plt.savefig("mode_hist.png")
				#　modeSet　蒐集所有大於　mode_hist數量最多區間　小於mode_hist數量最多區間的下一區間　
			modeSet = mode_count[(mode_count[:]>=mode_hist[1][np.argmax(mode_hist[0])]) &
			 (mode_count[:]<=mode_hist[1][np.argmax(mode_hist[0])+1])] 			
			sum_mode = round(np.mean(modeSet), 5)
		else:
			sum_mode = np.argmax(np.bincount(mode_count))	
		#plt.savefig("./processData/hist2.png")	
		plt.close()
		return sum_mode

	def run(self, newHeight, ratio, firstMode, low_standard):
		line = 255 - self.line
		line = (line - line[0]) ** 2 / np.std(line)
		line = self.checkMinValue(line)
		line = self.avergy_moving(line, self.mode, self.stride)
		break_line = int(len(line) / 5)
		sum_mode = firstMode
		if sum_mode<30:           #threshold 微調
			newH=np.mean(line) *0.8
		else:
			newH=250
		new_dis = int(sum_mode * ratio)
		n_peaks1, _ = find_peaks(line, height=newH, distance=new_dis)
		diff_line = 255 * np.ones(len(line))
		diff_line[n_peaks1] = 0



		ret2, line2 = cv2.threshold(diff_line, 100, 255, cv2.THRESH_BINARY)
		sum_count = np.array(self.countLine(line2))
		# mode_count = np.array(self.countLine(line2[break_line:]))
		mode_count = np.array(self.countLine(line2))
		sum_mode = self.selectMode(mode_count)
		sum_list = sum_count / sum_mode
		# print("endMode: ",sum_mode)

		# print(sum_list)
		return sum_list, n_peaks1, sum_mode

	def visualPeak2(self,ori_line, GapDict, sum_list, n_peaks1, leastMode):
		h = n_peaks1[-1] - n_peaks1[0]
		peakList =[n_peaks1[0]]
		peak_interval_list = [sum_list[i]/sum(sum_list)*h for i in range(sum_list.shape[0])]
		for i in range(sum_list.shape[0]):
			peakList.append(int(n_peaks1[0] + sum(peak_interval_list[:i+1])))



		return peakList	

	def visualGap(self,gapIndex):
		gapList =[]
		gapArray=[[0,0]]
		#######生成gap區間　［gapStartPoint, gap_length］
		if len(gapIndex) !=0:
			for gap in gapIndex:
				if gapList==[]:
					gapList.append(gap)
				else: 
					if gapList[-1] +1 == gap:
						gapList.append(gap)
					else: 
						gap_len = gapList[-1]-gapList[0]
						gapArray.append([gapList[0],gap_len])
						gapList=[]
						gapList.append(gap)
			gap_len = gapList[-1]-gapList[0]
			gapArray.append([gapList[0], gap_len])		
			return gapArray
		else:
			return []


	def getPCBNumber(self,sum_list,n_peaks1,gapArray, peakMode):
		label="normal"
		peakMode = peakMode *0.96
		peakTotalInterval = n_peaks1[-1] - n_peaks1[0]
		peakList = [int(np.sum(sum_list[:i])/sum(sum_list)*peakTotalInterval + n_peaks1[0]) for i in range(sum_list.shape[0] + 1)]
		# print(n_peaks1[0],peakList[0])
		# print(sum_list[114:117])
		newPeakList=[]
		coverList = []
		errorList = []
		coverCount = 0
		temp = 0
		newPeakList.append(peakList[0])
		# FIRST PLATE
		for i in range(len(peakList)-1):
			peakLower = peakList[i]
			peakUpper = peakList[i + 1]
			peakInterval = sum_list[i]
			peakUpper_hat = peakUpper
			peakInterval_hat = peakInterval
			for gap in gapArray:
				gapLower = gap[0]; gapInterval = gap[1]
				if (gapLower > peakLower) and (gapLower < peakUpper):
					peakUpper_hat = peakUpper - gapInterval
					peakInterval_hat = peakInterval - gapInterval/peakMode				
			if (peakInterval_hat*100%100 >= 35) and (peakInterval_hat*100%100 <= 65):
				if (i == 0) and (peakInterval_hat < 1.6):
					newPeakList.append(peakUpper_hat)
				else:
					numCover = int(peakInterval_hat + temp)
					coverCount = coverCount + numCover
					for k in range(numCover):
						coverLine = peakLower - temp*peakMode + peakMode*(k+1)
						coverList.append(coverLine)
					temp = max(peakInterval_hat + temp - numCover, 0)
				# print(peakInterval_hat, numCover, temp, "temp")
				if (temp > 0.7):
					coverList.append(coverLine + peakMode)
					temp = 0 
			else:
				err_condition = ((peakInterval_hat + temp)*100%100 > 40) and ((peakInterval_hat + temp)*100%100 < 55)
				if err_condition:
					# print("Error, some value are too ambiguous", i, peakInterval_hat + temp)
					errorList.append(peakLower)
					label = "ambiguous"

				numCover = int(round(peakInterval_hat + temp)-1)
				coverCount = coverCount + numCover
				for k in range(numCover):
					coverList.append(peakLower - temp*peakMode + peakMode*(k+1))
				newPeakList.append(peakUpper_hat)
				if coverCount>3:
					label = "ambiguous"
					errorList.append(peakUpper_hat)
				coverCount = 0
				temp = 0


		result = len(newPeakList) + len(coverList) - 1
		return coverList, result ,newPeakList, errorList, label


	def drawPeak(self,peakList,coverList,gapArray,errorList,oriImg,visImg,svdImg):
		# svd_img = cv2.imread("processData/5svd.png")
		# gap_img = cv2.imread("processData/gap.png")
		svd_img = cv2.cvtColor(svdImg.astype('float32'),cv2.COLOR_GRAY2BGR)
		gap_img =visImg
		h,w,_ = svd_img.shape
		whiteShape = (h, w, 3)	
		whiteImg = np.full(whiteShape, 255).astype(np.uint8)	
		for index in peakList:
			cv2.line(svd_img, (0,index), (w-1,index), (0,255,0), 3)		
			cv2.line(whiteImg, (0,index), (w-1,index), (0,255,0), 3)		
		for cover in coverList:
			cv2.line(svd_img, (0,int(cover)), (w-1,int(cover)), (255,50,50), 3)		
			cv2.line(whiteImg, (0,int(cover)), (w-1,int(cover)), (255,50,50), 3)
		for gap_set in gapArray:
			gapStart =  gap_set[0]
			gapEnd = gapStart + gap_set[1]
			cv2.rectangle(svd_img, (0,gapStart), (w,gapEnd), (0,0,255), -1)
			cv2.rectangle(whiteImg, (0,gapStart), (w,gapEnd), (0,0,255), -1)	
		for error in errorList:
			cv2.line(svd_img, (0,int(error)), (w-1,int(error)), (0,77,255), 3)		
			cv2.line(whiteImg, (0,int(error)), (w-1,int(error)), (0,77,255), 3)		
		cv2.line(oriImg,(0,int(h*0.975)),(w-1,int(h*0.975)),(0,0,255),3)	
		outputImg = np.hstack((gap_img,svd_img,whiteImg,oriImg))	
		#cv2.imwrite("processData/output.png",outputImg)
		return outputImg

