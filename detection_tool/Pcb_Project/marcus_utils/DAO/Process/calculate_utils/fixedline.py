import numpy as np
from tkinter import _flatten

class fixedLine_Class:
    def setFixedLine(self ,line ,firstPeak ,endPeak ,firstMode, GapDict, GapStandard):
        self.line = line[firstPeak:endPeak]
        self.GapDict = GapDict
        self.firstMode = firstMode
        self.firstPeak =firstPeak
        self.gapStandard = GapStandard
    def findGap(self):
        maximum=len(self.line)
        lst=np.array(self.line)
        ind=lst==0
        indexArray=np.arange(0,maximum)
        #記錄所有黑色的index
        blackArray = indexArray[ind]
        threshold = self.gapStandard
        count =0
        countlist=[]
        sumCountList=[]
        for i in range(len(blackArray)):
            count = count +1
            countlist.append(blackArray[i]+self.firstPeak)
            if i == len(blackArray)-1:
                if count >threshold :
                    sumCountList.append(countlist)
                    count=0
                    countlist=[]
                else:
                    count=0
                    countlist=[]                        
            elif blackArray[i]+1 != blackArray[i+1]:
                if count >threshold :
                    sumCountList.append(countlist)
                    count=0
                    countlist=[]
                else:
                    count=0
                    countlist=[]
        self.sumCountList =sumCountList
    def _chooseCorrectGap(self):
        correctGapList = []
        for gap in self.GapDict:
            bound = self.GapDict["{}".format(gap)]

            minBound = bound[0]
            maxBound = bound[1]
            for countList in self.sumCountList:
                count = countList
                downindex = count>=minBound
                count =np.array(count)[downindex]
                upindex = count<=maxBound
                count = np.array(count)[upindex]
                if len(count) != 0:
                    correctGapList.append(countList)
        correctGapList = np.array(list(_flatten(correctGapList))) 

        return correctGapList    