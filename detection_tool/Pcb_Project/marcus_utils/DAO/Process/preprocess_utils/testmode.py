import cv2
import numpy as np
import matplotlib.pyplot as plt
from scipy.signal import find_peaks
import json
#以測試的方式先找到peak's mode 的大致數值並返回
class TestMode_Class:
	def __init__(self):
		self.mode = 5
		self.stride = 1 
	def __call__(self,line):
		# lineDict = {}
		# lineList=[]
		line = 255 - line
		line = (line-line[0])**2/np.std(line)
		line = self._checkMinValue(line)
		line =self._avergy_moving(line,self.mode,self.stride)		
		#low_standard = min(int(np.max(line)*0.3),int(np.mean(line)*3)) 
		low_standard = int(np.max(line)*0.3)
		peaks1, _ = find_peaks(line, height=low_standard, distance=10)
		diff_line = 255*np.ones(len(line))
		diff_line[peaks1] = 0
		# lineDict["line"]= line
		# lineDict["diffLine"]=diff_line
		# lineList.append(lineDict)
		# print(type(line[0]))
		firstPeak, endPeak= peaks1[0], peaks1[-1]
		ret2,line2 = cv2.threshold(diff_line,100,255,cv2.THRESH_BINARY)
		mode_count = np.array(self._countLine(line2))
		sum_mode = self._selectMode(mode_count)
		# print("firstMode: ",sum_mode)
		# with open("./processData/peak.json",'w') as f:
		# 	json.dump(line, f)		
		return [sum_mode,low_standard,firstPeak, endPeak]		
	def _countLine(self,line):
		last = 255
		count_inner = 0
		ind_black = np.arange(len(line)-1) 
		ind_black = ind_black[(np.diff(line[:,0],n=1) == -255)]

		ind_white = np.arange(len(line)-1) 
		ind_white = ind_white[(np.diff(line[:,0],n=1) == 255)]
		ListNum = []
		for i in range(len(ind_black)-1):
			ListNum.append(ind_black[i+1] - ind_black[i])
		return ListNum

	def _checkMinValue(self,line):
		min_val = np.min(line)
		if min_val<0:
			line = np.array(line)
			new_array = line-min_val
			new_line = new_array.tolist()
		else:
			new_line = line
		return new_line	
	def _avergy_moving(self,line,mode,stride):
		new_line=[]
		line = np.square(line)
		for i in range(len(line)-mode+1):
			avergy_value =np.mean(line[i:i+mode]) 
			new_line.append(np.sqrt(avergy_value))
		return new_line	
	def _selectMode(self,mode_count): 
		num_bins = 10
		bins=int((np.max(mode_count)-np.min(mode_count))/num_bins)  
		if bins!=0:
			mode_hist= plt.hist(mode_count,bins=bins)    
			modeSet = mode_count[(mode_count[:]>=mode_hist[1][np.argmax(mode_hist[0])]) & (mode_count[:]<=mode_hist[1][np.argmax(mode_hist[0])+1])] 
			sum_mode= round(np.mean(modeSet),3)
			#sum_mode = mode_hist[1][np.argmax(mode_hist[0])]
		else:
			sum_mode = np.argmax(np.bincount(mode_count))	
		# print(mode_hist)	
		# plt.savefig("./processData/hist.png")
		return sum_mode