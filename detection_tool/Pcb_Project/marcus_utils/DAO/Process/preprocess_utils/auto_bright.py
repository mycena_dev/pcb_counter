import cv2
import numpy as np

class Auto_bright_contrast_Class:
	def __init__(self):
		self.clip_hist_percent=25
	def setImg(self,img):
		self.img = img
	def convertScale(self,img, alpha, beta):
		new_img = img * alpha + beta
		new_img[new_img < 0] = 0
		new_img[new_img > 255] = 255
		return new_img.astype(np.uint8)
	def run(self, img):
		gray = img
   		 # Calculate grayscale histogram
		hist = cv2.calcHist([gray],[0],None,[256],[0,256])
		hist_size = len(hist)
		# Calculate cumulative distribution from the histogram
		accumulator = []
		accumulator.append(float(hist[0]))
		for index in range(1, hist_size):
			accumulator.append(accumulator[index -1] + float(hist[index]))
    		# Locate points to clip
		maximum = accumulator[-1]
		self.clip_hist_percent=25
		self.clip_hist_percent *= (maximum/100.0)
		self.clip_hist_percent /= 2.0
   		# Locate left cut
		minimum_gray = 0
		while accumulator[minimum_gray] < self.clip_hist_percent:
			minimum_gray += 1
    		# Locate right cut
		maximum_gray = hist_size -1
		while accumulator[maximum_gray] >= (maximum - self.clip_hist_percent):
			maximum_gray -= 1
		# Calculate alpha and beta values
		alpha = 255 / (maximum_gray - minimum_gray)
		beta = -minimum_gray * alpha
		auto_result = self.convertScale(img, alpha=alpha, beta=beta)
		self.setImg(auto_result)
		return auto_result	
	def getImg(self):	
		return self.img
	def saveImg(self, saveDirPath:str , name: str):	
		cv2.imwrite(saveDirPath + "/" + name + ".png", self.img)			
