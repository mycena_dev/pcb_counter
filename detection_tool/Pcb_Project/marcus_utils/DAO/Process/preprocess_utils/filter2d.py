import cv2
import numpy as np


class Filter2d_Class:

	def set_proportion_h(self,proportion_h):
		self.proportion_h = proportion_h
	def setImg(self,img):
		self.img =img
	def run(self, img):
		kernel=np.array((
			[0,1,0],
			[1,-4,1],
			[0,1,0]),dtype="float32")
		kernel=np.array((
			[1,1,1],
			[0,0,0],
			[-1,-1,-1]),dtype="float32")
		img_u = cv2.filter2D(img,-1,kernel)
		kernel=np.array((
			[-1,-1,-1],
			[0,0,0],
			[1,1,1]),dtype="float32")
		img_d = cv2.filter2D(img,-1,kernel)
		kernel=np.array((
			[1,0,-1],
			[1,0,-1],
			[1,0,-1]),dtype="float32")
		img_l = cv2.filter2D(img,-1,kernel)
		kernel=np.array((
			[-1,0,1],
			[-1,0,1],
			[-1,0,1]),dtype="float32")
		img_r = cv2.filter2D(img,-1,kernel)
		h,w = img.shape
		img_f = np.zeros((h,w))
		img_f[0:int(h*self.proportion_h),:] = img_d[0:int(h*self.proportion_h),:]
		img_f[int(h*self.proportion_h):,:] = img_u[int(h*self.proportion_h):,:]
		img_f = img_f.astype(np.uint8)
		self.setImg(img_f)
		return img_f		
	def getImg(self):	
		return self.img
	def saveImg(self, saveDirPath:str , name: str):	
		cv2.imwrite(saveDirPath + "/" + name + ".png", self.img)			
