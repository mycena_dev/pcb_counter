import cv2
import numpy as np

class Crop_Class:
    def __init__(self):
        pass

    def setInputImg(self, oriImg, img, th2):
        self._setOriImg(oriImg) 
        self._setImg(img)
        self._setTh2(th2)

    def setCropCoordinate(self, coordinate):
        h, w = self.img.shape
        self.startPoint, self.endPoint = int(coordinate[0]*w), int(coordinate[1]*w)

    def _drawCropLine(self, img):
        img[:,self.startPoint-1:self.startPoint+5] =0
        img[:,self.endPoint-5:self.endPoint] =0
        return img

    def saveImg(self, saveDirPath:str , name: str):	
        self.drawImg = self._drawCropLine(self.img)
        img = cv2.cvtColor(self.drawImg,cv2.COLOR_GRAY2BGR)
        cv2.imwrite(saveDirPath + "/" + name + ".png", img)  

    def getCropImg(self):
        drawImg = self._drawCropLine(self.img)
        return drawImg[:,self.startPoint:self.endPoint]

    def getCropTh2(self):
        return self.th2[:,self.startPoint:self.endPoint]

    def getCropOriImg(self):
        return self.oriImg[:,self.startPoint:self.endPoint]  


    def _setImg(self, img):
        self.img =img

    def _setTh2(self, th2):
        self.th2 =th2

    def _setOriImg(self,oriImg):
        self.oriImg =oriImg 