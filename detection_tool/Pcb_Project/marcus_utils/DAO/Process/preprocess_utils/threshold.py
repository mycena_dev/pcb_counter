import cv2
class Threshold_Class:
    def setImg(self,img):
        self.img =img
    def run(self,img):
        _,th2 = cv2.threshold(img,70,255,cv2.THRESH_BINARY)
        self.setImg(th2)
        return th2
    def getImg(self):	
        return self.img
    def saveImg(self, saveDirPath:str , name: str):	
        cv2.imwrite(saveDirPath + "/" + name + ".png", self.img)       
