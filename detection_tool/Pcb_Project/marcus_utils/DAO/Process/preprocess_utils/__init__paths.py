import os.path as osp
import sys

def add_path(path):

    if path not in sys.path:
        sys.path.insert(0, path)

this_dir = osp.dirname(__file__)

# Add lib to PYTHONPATH

lib_path = osp.join(this_dir, '/Downloads/OPC_3000/crop_object/marcus/S_ver6/allen_utils')
add_path(lib_path)
