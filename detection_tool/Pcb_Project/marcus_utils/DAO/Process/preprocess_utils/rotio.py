import numpy as np
class RatioLabel_Class:
	def __init__(self):
		pass
	def selectRatio(self,label):
		if label == "RightDiagonal":
			start = 0.475
			end = 0.525
		elif label == "LeftDiagonal":
			start = 0.475
			end = 0.525
		else:
			start = 0.475
			end = 0.525
		return start,end
	def setRatio(self,startRatio,endRatio):
		self.startRatio = startRatio
		self.endRatio = endRatio
	def getRatio(self):
		return [self.startRatio, self.endRatio]