import cv2
import numpy as np
import math


class find_center_Class:
	def setImg(self,img,startPoint,endPoint):
		self.img= img
		self.startPoint= startPoint
		self.endPoint= endPoint
	
	def grabcut_img(self,ori_img):
		#ori_img = cv2.cvtColor(ori_img, cv2.COLOR_GRAY2RGB)
		h,w,_= ori_img.shape
		n_h = int(h/10)
		n_w = int(w/10)
		img=cv2.resize(ori_img,(n_w,n_h))
		mask = np.zeros(img.shape[:2],np.uint8)
		bgdModel = np.zeros((1,65),np.float64)
		fgdModel = np.zeros((1,65),np.float64)
		rect = (1,1,n_w-1,n_h-1)
		cv2.grabCut(img,mask,rect,bgdModel,fgdModel,1,cv2.GC_INIT_WITH_RECT)
		mask2 = np.where((mask==2)|(mask==0),0,1).astype('uint8')
		mask2 = mask2*255
		mask2 = cv2.resize(mask2,(w,h))
		# cv2.imwrite("./processData/mask.png",mask2)
		center_line = self.find_center(mask2)
		return center_line
	def find_center(self,img):
		H,W = img.shape
		line = img[:,int((self.startPoint+self.endPoint)/2*W)]
		h=len(line) 
		i1=0
		i2=h-1
		y1,y2=0,0
		for k in range(h):
			point = line[i1+k]
			if point !=0:
				y1=i1+k
				break
		for l in range(h):
			point = line[i2-l]
			if point !=0:
				y2=i2-l
				break
		upline=math.floor((y1/h)*1000)*0.001
		downline=math.floor((y2/h)*1000)*0.001               
		center = (int((y2-y1)/2)+y1)/h
		center = math.floor(center*1000)*0.001
		return center,upline,downline

	def run(self):
		return self.grabcut_img(self.img)
	
