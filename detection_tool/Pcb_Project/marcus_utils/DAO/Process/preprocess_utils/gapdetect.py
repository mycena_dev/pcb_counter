import cv2
import numpy as np
from tkinter import _flatten

class gapDetect_Class:
    def __init__(self):
        self.stdRatio, self.testRatio = self._generateRatio()
        
    def __call__(self,th2,startIndex,endIndex, gapStandard):
        img = self._setTH2(th2,startIndex,endIndex)
        boundSet = self._findStandardGap(self.stdRatio,img,gapStandard)
        if len(boundSet) != 0:
            sumCountList =self._findGap(img,self.threshold,self.testRatio)
            GapDict = self._chooseCorrectGap(boundSet,sumCountList)
            return GapDict
        else:
            return {}


    def _generateRatio(self):
        ratio =np.arange(0.1,1,0.2)
        standardRatio = ratio[2]
        testRatio = np.delete(ratio,2)
        self.num_ratioLine = testRatio.shape[0]
        return [standardRatio, testRatio]

    def _setTH2(self,th2,startIndex,endIndex):
        return th2[startIndex:endIndex,:]
        
    def _findStandardGap(self, ratio, img, gapStandard):
        threshold =  gapStandard
        self.threshold = threshold
        # print("threshold: ",threshold)
        h, w = img.shape 
        StandardLine = img[:,int(ratio * w)]
        ind = StandardLine==0
        indexArray = np.arange(0,len(StandardLine))
        count =0
        countlist=[]
        sumCountList=[]
        #遍歷所有黑點，如黑點index連續次數超過thrsehold，則將該黑點連續區間index蒐集，當作縫隙候選。
        for i in range(len(indexArray[ind])):
            count = count +1
            countlist.append(indexArray[ind][i])
            if i == len(indexArray[ind])-1:
                if count >threshold  :
                    sumCountList.append([min(countlist),max(countlist)])
            elif indexArray[ind][i]+1 != indexArray[ind][i+1]:
                if count >threshold  :
                    sumCountList.append([min(countlist),max(countlist)])
                    count=0
                    countlist=[]
                else:
                    count=0
                    countlist=[]
        boundSet = sumCountList

        return boundSet
    def _findGap(self,img,threshold,ratio):
        count =0
        countlist=[]
        sumCountList=[]
        _,w = img.shape
        for r in ratio : 
            line = img[:,int(r*w)]
            ind = line==0
            indexArray=np.arange(0,len(line))
            blackArray = indexArray[ind]
            # print(blackArray)
            for i in range(len(blackArray)):
                count = count +1
                countlist.append(blackArray[i])
                if i == len(blackArray)-1:
                    if count >threshold :
                        sumCountList.append(countlist)
                        count=0
                        countlist=[]
                    else:
                        count=0
                        countlist=[]                        
                elif blackArray[i]+1 != blackArray[i+1]:
                    if count >threshold :
                        sumCountList.append(countlist)
                        count=0
                        countlist=[]
                    else:
                        count=0
                        countlist=[]
        #     print("===================================",r)
        # print("sumCountList: ",sumCountList)
        return sumCountList
    def _chooseCorrectGap(self,boundSet,sumCountList):
        GapDict={}
        id= 1
        for bound in boundSet:
            minBound = bound[0]
            maxBound = bound[1]
            gapSet=[]
            gapLineCounter = 0
            for countList in sumCountList:
                # print("counterList: ",countList)
                count = countList
                downindex = count>=minBound
                count =np.array(count)[downindex]
                upindex = count<=maxBound
                count = np.array(count)[upindex]
                #以中線算出的bound為基準，查看每條test線的黑色index是否有在中線bound內
                if count.shape[0] !=0:
                    gapLineCounter = gapLineCounter+1
                    gapSet.append(countList)
                    # GapDict["gap{}".format(id)]
                    # print("===============countlist: ",count)
                #如果四條線都有在中線bound內，則可認為該bound內index為板縫
                if gapLineCounter ==self.num_ratioLine:
                    # print(list(_flatten(gapSet)))
                    finalDownLine = min(list(_flatten(gapSet)))
                    finalUpLine = max(list(_flatten(gapSet)))
                    GapDict["id{}".format(id)]=[finalDownLine,finalUpLine]
                    id = id+1
                    gapLineCounter=0
                    gapSet=[]
        return GapDict
        # print(GapDict)

