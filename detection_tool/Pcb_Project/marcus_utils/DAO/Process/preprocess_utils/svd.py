import cv2
import numpy as np
class SVD_Class:
	def setImg(self,img):
		self.img = img
	def run(self,img):
		u,s,v = np.linalg.svd(img)
		img_out = 0
		a = s/s[0]
		kk = np.ones(len(a))
		ind = a>0.1
		s_iter = int(sum(kk[ind]))
		for ii in range(s_iter):
			img_out = img_out + s[ii]*np.outer(u[:,ii],v[ii,:])
		self.setImg(img_out)	
		return img_out
	def getImg(self):	
		return self.img
	def saveImg(self, saveDirPath:str , name: str):	
		cv2.imwrite(saveDirPath + "/" + name + ".png", self.img)
