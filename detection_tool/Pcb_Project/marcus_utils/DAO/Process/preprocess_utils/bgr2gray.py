import cv2
class Bgr2gray_Class:
	def setImg(self,img):
		self.img = img
	def run(self, inputImage):
		grayImg = cv2.cvtColor(inputImage,cv2.COLOR_BGR2GRAY)
		self.setImg(grayImg)
		return grayImg
	def getImg(self):	
		return self.img
	def saveImg(self, saveDirPath:str , name: str):	
		cv2.imwrite(saveDirPath + "/" + name + ".png", self.img)
