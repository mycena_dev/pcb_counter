import numpy as np
import cv2
class Dilate_Class:
	def __init__(self):
		self.kernel = np.ones((1,5),np.uint8)
	def setImg(self,img):
		self.img =img
	def bitwise_not(self,img):
		return cv2.bitwise_not(img)
	def run(self, img):
		dilate_img = cv2.dilate(img,self.kernel,iterations = 1)
		bitwise_not_img = self.bitwise_not(dilate_img)
		self.setImg(bitwise_not_img)
		return bitwise_not_img
	def getImg(self):	
		return self.img
	def saveImg(self, saveDirPath:str , name: str):	
		cv2.imwrite(saveDirPath + "/" + name + ".png", self.img)		
		
