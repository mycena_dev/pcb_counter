from DAO.Process.preprocess_utils.bgr2gray import Bgr2gray_Class
from DAO.Process.preprocess_utils.auto_bright import Auto_bright_contrast_Class
from DAO.Process.preprocess_utils.filter2d import Filter2d_Class
from DAO.Process.preprocess_utils.dilate import Dilate_Class
from DAO.Process.preprocess_utils.svd import SVD_Class 
from DAO.Process.preprocess_utils.find_center import find_center_Class
from DAO.Process.preprocess_utils.rotio import RatioLabel_Class
from DAO.Process.preprocess_utils.threshold import Threshold_Class
from DAO.Process.preprocess_utils.testmode import TestMode_Class
from DAO.Process.preprocess_utils.gapdetect import gapDetect_Class
from DAO.Process.preprocess_utils.crop import Crop_Class
import cv2
import numpy as np
class ImgPreprocess_Class:
	def __init__(self):
		self.find_center=find_center_Class()
		self.bgr2gray = Bgr2gray_Class()
		self.auto_bright = Auto_bright_contrast_Class()
		self.filter2d = Filter2d_Class()
		self.dilate = Dilate_Class()
		self.ratio = RatioLabel_Class()
		self.svd = SVD_Class()
		self.threshold= Threshold_Class()
		self.testmode = TestMode_Class()
		self.gapdetect = gapDetect_Class()
		self.crop = Crop_Class()
		self.w, self.h = 4000,4000
		# self.saveDirPath = "./processData"


	def run(self, inputImage, label):
		#init set
		startPoint,endPoint= self.ratio.selectRatio(label)
		self.find_center.setImg(inputImage,startPoint,endPoint)
		self.center_line, self.upline, self.downline= self.find_center.run()	
		self.select_proportion_h(label)

		#saveInput
		img =self.bgr2gray.run(inputImage)
		# self.bgr2gray.saveImg(self.saveDirPath,"1ori")

		#Adjust contrast
		img =self.auto_bright.run(img)

		th2 =self.threshold.run(img)

		self.filter2d.set_proportion_h(self.proportion_h)
		# self.filter2d.setImg(img)
		img = self.filter2d.run(img)
		# self.filter2d.saveImg(self.saveDirPath,"2filter")

		# self.dilate.setImg(img)
		img = self.dilate.run(img)

		img = cv2.resize(img ,(self.w,self.h))
		oriImg =cv2.resize(inputImage ,(self.w,self.h))
		th2 = cv2.resize(th2 ,(self.w,self.h))
		# self.threshold.saveImg(self.saveDirPath,"3th1")

		self.crop.setInputImg(oriImg=oriImg, img=img, th2=th2)
		self.crop.setCropCoordinate(coordinate=[startPoint,endPoint])
		# self.crop.saveImg(self.saveDirPath,"4CropLine")
		cropImg = self.crop.getCropImg()
		th3 = self.crop.getCropTh2()
		oriImg = self.crop.getCropOriImg()
		
		# self.svd.setImg(cropImg)
		svdImg =self.svd.run(cropImg)
		svdImg = self._insertLossPixel(svdImg, self.center_line)	
		# self.svd.saveImg(self.saveDirPath,"5svd")
		# if saveImgPath is not None:
		# 	self.bgr2gray.saveImg(saveImgPath,"1ori")
		# 	self.filter2d.saveImg(saveImgPath,"2filter")
		# 	self.threshold.saveImg(saveImgPath,"3th1")
		# 	self.crop.saveImg(saveImgPath,"4CropLine")
		# 	self.svd.saveImg(saveImgPath,"5svd")

	### get first mode     取圖片中線做為mode檢測線。
		line=svdImg[:,int(0.5*svdImg.shape[1])]
		firstMode,low_standard,firstPeak, endPeak = self.testmode(line)
	#### get Gap Dict
		intStartPoint,intEndPoint=int(self.w*startPoint),int(self.w*endPoint)
		gapStartIndex, gapEndIndex = 0, endPeak         #設定 檢測gap 的起始點與終點
		GapStandard = self._getGapStandard(firstMode)   #設定 Gap標準值，當大於標準即為Gap
		th4 = th2[:,intStartPoint-50:intEndPoint+50]  	#將th2加寬，作為GapDetect輸入
		GapDict = self.gapdetect(th4 ,gapStartIndex ,gapEndIndex ,GapStandard)   #Gap檢測並輸出字典
	###Visualization
		self._visualizationGap(svdImg, th3, gapStartIndex, gapEndIndex, GapDict)
	
		return [svdImg, th3, gapStartIndex, gapEndIndex, GapStandard, firstMode, GapDict, low_standard , oriImg]


	def _visualizationGap(self, svd, th3, startLine, endLine, GapDict):
		svd = svd.astype('float32')
		visImg = np.hstack((th3,svd))		
		visImg = cv2.cvtColor(visImg,cv2.COLOR_GRAY2BGR)
		vis_h,vis_w,_=visImg.shape
		cv2.line(visImg, (0,startLine), (vis_w,startLine), (0,255,0), 3)
		cv2.line(visImg, (0,endLine), (vis_w,endLine), (0,255,0), 3)	

		for gap in GapDict:
			bound = GapDict["{}".format(gap)]
			y1,y2 = bound[0]+startLine, bound[1]+startLine
			cv2.line(visImg, (0,y1),(vis_w,y1) ,(0,0,255), 2)
			cv2.line(visImg, (0,y2),(vis_w,y2) ,(0,0,255), 2)	
			
		cv2.line(visImg, (0,int(self.center_line*vis_h)),(vis_w,int(self.center_line*vis_h)) ,(0,100,0), 3)	
		cv2.imwrite(self.saveDirPath + "/gap.png",visImg)
	
	def select_proportion_h(self,label):
		if label == 'panel':
			self.proportion_h = self.center_line
		else: 
			self.proportion_h=self.center_line

	#設定gap標準值，當版片太小時，輸出較小的標準值
	def _getGapStandard(self,mode):
		if mode > 30 : 
			GapStandard = 5
		else:
			GapStandard = 3
		return GapStandard	


	#圖片在進行filter處理時，以中線為原點，分別對上下部分進行上微分與下微分動作，會導致在中線部分因上下微分操作讓該區域遭到壓縮，故以下函式去補上了中線區域所缺失部分。
	def _insertLossPixel(self,img, centerLine):
		img = img.astype('float32')
		# img = cv2.cvtColor(img,cv2.COLOR_GRAY2BGR)
		
		h, w= img.shape
		centerLine= int(centerLine * h)
		newImgShape = (h, w)	
		newImg = np.full(newImgShape, 255).astype(np.float32)		
		newImg[:centerLine,:] = img[:centerLine,:]
		newImg[centerLine+5:,:] = img[centerLine:-5,:]
		newImg[:,:5] = 0
		newImg[:,-5:] = 0	
		return newImg
	

	
