from DAO.Process.calculate_utils.findpeak import findPeak_Class
from DAO.Process.calculate_utils.countplate import countPlate_Class
from DAO.Process.calculate_utils.fixedline import fixedLine_Class
import numpy as np
import cv2
import random
class PCB_calculate_Class:
	def __init__(self):
		self.findPeak = findPeak_Class()
		self.fixedLine = fixedLine_Class()
		self.test_ration = (np.arange(0,1,0.1))[4:]
		
	def run(self, PreprocessImgList,height,ratio,state):
		inputImage, th2, firstPeak, endPeak, firstMode, GapDict= PreprocessImgList
		result_img = cv2.cvtColor(inputImage.astype(np.float32),cv2.COLOR_GRAY2RGB)
		h,w = inputImage.shape
		countList = []
		cnt = 0
		print("Find {} Gap!! ".format(len(GapDict)))

		for (i,r) in enumerate(self.test_ration):
			ori_line =inputImage[:,int(r*w)]
			if state == "Turn on":
				if len(GapDict) != 0:
					th2Line=th2[:,int(r*w)]
					self.fixedLine.setFixedLine(th2Line,firstPeak, endPeak,firstMode,GapDict)
					self.fixedLine.findGap()
					gapIndex = self.fixedLine._chooseCorrectGap()
					line=inputImage[:,int(r*w)]
					line=np.delete(line, gapIndex)
				else:
					line=ori_line
			elif state =="Turn off":
				line=ori_line
			self.findPeak.setLine(line)
			sum_list,n_peaks1,leastMode = self.findPeak.run(height,ratio,firstMode)

			# print(sum_list)
			# for i in sum_list:
			# 	cnt = round(i) + cnt
			self.countPlate = countPlate_Class(sum_list)
			self.countPlate.select_countplate()
			sheet_count = self.countPlate.run()
			countList.append(sheet_count)
		# print(cnt,countList)			
		##########=================visualization================##########
			if i ==1:
				self.findPeak.visualPeak(ori_line,GapDict,firstPeak,sum_list,firstMode,n_peaks1,leastMode)

		






		return np.argmax(np.bincount(countList))
