import cv2

class Image:
    def __init__(self):
        self._image = 0

    def setImageByPath(self, imgPath):
        self._image = cv2.imread(imgPath)
        #self._image = cv2.rotate(image,cv2.cv2.ROTATE_90_CLOCKWISE)

    def getImage(self):
        return self._image