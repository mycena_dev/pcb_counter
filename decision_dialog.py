from PyQt5.QtWidgets import QDialog
import uis.yes_or_no


class DecisionDialog(QDialog):
    def __init__(self, parent=None):
        super().__init__(parent)
        self.ui = uis.yes_or_no.Ui_Dialog()
        self.ui.setupUi(self)
