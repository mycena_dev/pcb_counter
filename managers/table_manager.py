import os
import time
import uuid
from datetime import datetime

import cv2
import pandas as pd
import requests
from PyQt5.QtCore import Qt
from PyQt5.QtWidgets import QTableWidget, QPushButton, QToolButton, QLineEdit, QFileDialog, QTableWidgetItem, QLabel, \
    QHeaderView, QDialogButtonBox, QCheckBox

import decision_dialog
import message_dialog
from managers.config_manager import ConfigManager, FieldComponent
from managers.manager_hub import ManagerHub


class TableManager:
    def __init__(self, job_num: str,
                 save_folder_line_edit: QLineEdit,
                 save_folder_tool_button: QToolButton,
                 qtable: QTableWidget,
                 record_button: QPushButton,
                 pcb_number_label: QLabel,
                 upload_checkbox: QCheckBox,
                 update_recode_button: QPushButton):
        self.update_recode_button = update_recode_button
        self.update_recode_button.clicked.connect(self.update_recode_data)
        self.upload_checkbox = upload_checkbox
        self.configManager: ConfigManager = ManagerHub.config_manager
        self.pcb_number_label = pcb_number_label
        self.job_num = job_num
        date_str = datetime.now().strftime("%Y%m%d%H%M")
        self.save_folder_path = os.path.join(self.configManager.system_export_path_edit.text(), '') + date_str
        self.select_list = []
        self.save_folder_line_edit = save_folder_line_edit
        self.save_folder_line_edit.setText(date_str)
        self.save_folder_tool_button = save_folder_tool_button
        self.save_folder_tool_button.clicked.connect(self.select_save_folder)
        self.qtable = qtable
        selectionModel = self.qtable.selectionModel()
        selectionModel.selectionChanged.connect(self.trigger_recode_change)
        self.record_button = record_button
        self.record_button.clicked.connect(self.trigger_detection_and_record_data)
        self.init_table()
        self.need_update_data_row_with_ori_data = {}

    def trigger_recode_change(self):
        colNum = self.qtable.columnCount()
        try:
            row = self.qtable.currentRow()
            column = self.qtable.currentColumn()
            if column >= colNum-3:
                return
            if row not in self.need_update_data_row_with_ori_data:
                self.need_update_data_row_with_ori_data[row] = (self.qtable.item(row, i).text() for i in range(colNum-3))
            self.update_recode_button.setEnabled(True)
        except:
            pass

    def init_table(self):
        columnNum = 4
        columnNameList_ori = ["amount", "jobNumber", "date", "data_path"]
        columnNameList = []
        for fieldComponent in self.configManager.field_group_list:
            if fieldComponent.state:
                columnNum += 1
                columnNameList.append(fieldComponent.field_label.text())
        columnNameList += columnNameList_ori
        self.qtable.setColumnCount(columnNum)
        self.qtable.rowCount()
        self.qtable.setHorizontalHeaderLabels(columnNameList)
        # self.qtable.horizontalHeader().setSectionResizeMode(QHeaderView.Stretch)
        for i in range(self.qtable.columnCount()):
            self.qtable.horizontalHeader().setSectionResizeMode(i, QHeaderView.ResizeToContents)
        # self.qtable.setSizeAdjustPolicy(QAbstractScrollArea.AdjustToContents)
        # self.qtable.setSizePolicy(QSizePolicy.Expanding, QSizePolicy.Minimum)

    def select_save_folder(self):
        dialog = QFileDialog()
        folder_path = dialog.getExistingDirectory(None, "Select Folder",
                                                  self.configManager.system_export_path_edit.text())
        if folder_path != "":
            self.save_folder_path = folder_path
            folder_path = folder_path.replace(self.configManager.system_export_path_edit.text(), "")[1:]
            self.save_folder_line_edit.setText(folder_path)

    def get_the_index_of_first_one_enable_line_edit(self):
        result_index = -1
        for index, _fieldComponent in enumerate(self.configManager.field_group_list):
            fieldComponent: FieldComponent = _fieldComponent
            if fieldComponent.line_edit.isEnabled():
                result_index = index
                break
        return result_index

    def clear_data_and_reset_focus(self):
        for index, _fieldComponent in enumerate(self.configManager.field_group_list):
            fieldComponent: FieldComponent = _fieldComponent
            if fieldComponent.line_edit.isEnabled():
                fieldComponent.line_edit.clear()
        focus_line_edit_index = self.get_the_index_of_first_one_enable_line_edit()
        if focus_line_edit_index != -1:
            self.configManager.field_group_list[focus_line_edit_index].line_edit.setFocus()
    def writeLog(self, log_file_name, data):
        if not os.path.exists(log_file_name):
            columnHeaders = []
            for j in range(self.qtable.model().columnCount()):
                cell_content = self.qtable.horizontalHeaderItem(j).text().replace(',', '-')
                columnHeaders.append(cell_content)
            with open(log_file_name, 'a') as log_file:
                    log_file.writelines(','.join(columnHeaders) + '\n')
        with open(log_file_name, 'a') as log_file:
            log_file.writelines(','.join(data) + '\n')

    def addTableRow(self, row_data=None):
        colNum = self.qtable.columnCount()
        row = self.qtable.rowCount()
        self.qtable.setRowCount(row + 1)
        col = 0
        for item in row_data:
            cell = QTableWidgetItem(str(item))
            if col >= colNum-3:
                cell.setFlags(Qt.ItemIsEnabled)
            self.qtable.setItem(row, col, cell)
            col += 1
        if row == 300:
            self.qtable.removeRow(0)

    def trigger_detection_and_record_data(self):
        self.record_button.setEnabled(False)
        ManagerHub.detection_manager.trigger_detection()

    def record_data(self, filename):
        # image_filename = filename + '.jpg'
        image_filename = 'image.jpg'
        record_folder = os.path.join(self.save_folder_path, '') + filename
        if not os.path.exists(record_folder):
            os.makedirs(record_folder)
        # Integrate row data
        date = datetime.now().strftime("%Y/%m/%d %H:%M:%S")
        pcb_amount = self.pcb_number_label.text()
        base_data = [pcb_amount, self.job_num, date]
        if ManagerHub.detection_manager.current_image is not None:
            image_path = os.path.join(record_folder, '') + image_filename
            ManagerHub.detection_manager.current_image_encode.tofile(image_path)
            base_data.append(record_folder)
        field_value = []
        for _fieldComponent in self.configManager.field_group_list:
            fieldComponent: FieldComponent = _fieldComponent
            if fieldComponent.used_box.isChecked():
                field_value.append(fieldComponent.line_edit.text())
        all_row_data = field_value + base_data
        # Fill row data
        self.addTableRow(all_row_data)
        self.clear_data_and_reset_focus()
        self.qtable.scrollToBottom()
        csv_file_path = os.path.join(record_folder, '') + 'CSV.csv'
        columnHeaders = []
        # create column header list
        for j in range(self.qtable.model().columnCount()):
            columnHeaders.append(self.qtable.horizontalHeaderItem(j).text())

        df = pd.DataFrame(columns=columnHeaders)
        for index, data in enumerate(all_row_data):
            df.at[0, columnHeaders[index]] = data
        df.to_csv(csv_file_path, index=False)
        self.writeLog(os.path.join(self.save_folder_path, '') + 'logs.csv', all_row_data)
        if self.upload_checkbox.isChecked():
            url = ManagerHub.config_manager.upload_url_line_edit.text()
            _message_dialog = message_dialog.CustomDialog()
            csv_file = {'upload_file': open(csv_file_path, 'rb')}
            try:
                response = requests.post(url, files=csv_file)
                if response.status_code == 200:
                    _message_dialog.ui.message.setText("上傳完成")
                    _message_dialog.setWindowTitle("上傳完成")
                else:
                    _message_dialog.ui.message.setText("失敗！（未接收到Http 200 State）")
                    _message_dialog.setWindowTitle("錯誤")
                    _message_dialog.ui.buttonBox.setStandardButtons(QDialogButtonBox.Close)
            except:
                _message_dialog.ui.message.setText("無法辨識之URL")
                _message_dialog.setWindowTitle("錯誤")
                _message_dialog.ui.buttonBox.setStandardButtons(QDialogButtonBox.Close)
            _message_dialog.exec()
        return record_folder

    def update_csv_data(self, data_row):
        colNum = self.qtable.columnCount()
        csv_file_path = os.path.join(self.qtable.item(data_row, colNum-1).text(), '') + 'CSV.csv'
        columnHeaders = []
        # create column header list
        for j in range(self.qtable.model().columnCount()):
            columnHeaders.append(self.qtable.horizontalHeaderItem(j).text())
        df = pd.DataFrame(columns=columnHeaders)
        for j in range(self.qtable.model().columnCount()):
            df.at[0, columnHeaders[j]] = self.qtable.item(data_row, j).text()
        df.to_csv(csv_file_path, index=False)

    def update_log_file(self, path_key=None, df=None, data_row = None):
        target_row_idx = df.loc[df["data_path"] == path_key].index.values[0]
        for row_idx in range(df.shape[1]):
            # origin_value = row_data.iloc[0, row_idx]
            # new_value = self.qtable.item(data_row, row_idx).text()
            # print("row_idx : ", row_idx)
            # print("origin_value : ", origin_value)
            # print("new_value : ", new_value)
            # print('-------------------')
            df.iloc[target_row_idx, row_idx] = self.qtable.item(data_row, row_idx).text().replace(',', '-')

    def update_recode_data(self):
        colNum = self.qtable.columnCount()
        self.update_recode_button.setEnabled(False)
        log_file_path = os.path.join(self.save_folder_path, '') + 'logs.csv'
        df = pd.read_csv(log_file_path)
        for row, value in self.need_update_data_row_with_ori_data.items():
            new_data = (self.qtable.item(row, i).text() for i in range(colNum-3))
            if new_data != value:
                self.update_csv_data(row)
                self.update_log_file(path_key=self.qtable.item(row, colNum-1).text(), df=df, data_row =row)
        df.to_csv(log_file_path, index=False)
        self.need_update_data_row_with_ori_data = {}
