﻿import os

import cv2
from pypylon import pylon
import numpy as np
import requests
from PyQt5 import QtGui
from PyQt5.QtCore import QRect, QThread, pyqtSignal, Qt
from PyQt5.QtGui import QPixmap
from PyQt5.QtWidgets import QPushButton, QLineEdit, QCheckBox, QWidget, QLabel, QTabWidget, QToolButton, QFileDialog, \
    QDialogButtonBox, QSlider, QFrame
from tinydb import Query
import time
import decision_dialog
import message_dialog
from agents.db_agent import DBAgent as db
from domain.account import Account
from managers.manager_hub import ManagerHub
from managers.camera_manager import *

class VideoThread(QThread):
    change_pixmap_signal = pyqtSignal(np.ndarray)

    def __init__(self):
        super().__init__()
        self.running_flag = False

    # WebCamera
    # def run(self):
    #    img = cv2.imread('./detection_tool/Pcb_Project/demo/dataset/1.jpg')
    #    cv_img = cv2.rotate(img, cv2.cv2.ROTATE_90_CLOCKWISE)
    #    self.change_pixmap_signal.emit(cv_img)

    def stop(self):
        self.running_flag = False

    def run(self):
        if self.running_flag:
            return
        self.running_flag = True
        while Camera.IsGrabbing():
            grabResult = Camera.RetrieveResult(2000, pylon.TimeoutHandling_ThrowException)
            if grabResult.GrabSucceeded():
                img = grabResult.Array
                img = cv2.rotate(img, cv2.cv2.ROTATE_90_CLOCKWISE)
                self.change_pixmap_signal.emit(img)
            assert grabResult.GrabSucceeded() == True
            grabResult.Release()
            if not self.running_flag:
                break

class FieldComponent:
    def __init__(self, state: bool = None, field_label: QLabel = None, line_edit: QLineEdit = None,
                 lock_button: QPushButton = None, rename_line_edit: QLineEdit = None, used_box: QCheckBox = None,
                 field_group: QWidget = None):
        self.state = state
        self.field_label = field_label
        self.line_edit = line_edit
        self.lock_button = lock_button
        self.rename_line_edit = rename_line_edit
        self.used_box = used_box
        self.field_group = field_group


class FieldComponentValue:
    def __init__(self, index: int, fieldComponent: FieldComponent):
        self.index = index
        self.state = fieldComponent.state
        self.field_label_text = fieldComponent.field_label.text()
        self.rename_line_edit = fieldComponent.rename_line_edit.text()
        self.used_box = fieldComponent.used_box.isChecked()


class ConfigManager:
    def __init__(self, account: Account, tabWidget: QTabWidget,
                 save_config_button: QPushButton,
                 system_export_path_tool_button: QToolButton,
                 system_export_path_edit: QLineEdit,
                 upload_url_line_edit: QLineEdit,
                 upload_test_button: QPushButton, baseline_adjust_down: QPushButton, baseline_adjust_up: QPushButton,
                 baseline_adjust_slider: QSlider, baseline_adjust: QFrame, baseline_use_image: QLabel, save_process_image: QCheckBox):
        self.account = account
        self.baseline_ratio = 0.0
        self.field_group_list = []
        self.tabWidget = tabWidget
        self.return_config_tab = False
        self.baseline_use_image = baseline_use_image
        self.baseline_use_image.mousePressEvent = self.trigger_camera_activity
        self.save_process_image = save_process_image
        self.save_process_image.clicked.connect(self.enable_save_config_button)
        self.baseline_adjust = baseline_adjust
        self.baseline_y0 = self.baseline_adjust.geometry().y()
        # Define baseline_adjust_down
        self.baseline_adjust_down = baseline_adjust_down
        self.baseline_adjust_down.clicked.connect(lambda: self.trigger_baseline_adjust_slider('down'))
        # Define baseline_adjust_up
        self.baseline_adjust_up = baseline_adjust_up
        self.baseline_adjust_up.clicked.connect(lambda: self.trigger_baseline_adjust_slider('up'))
        # Define baseline_adjust_slider
        self.baseline_adjust_slider = baseline_adjust_slider
        self.baseline_adjust_slider.setMaximum(self.baseline_use_image.height())
        self.baseline_adjust_slider.valueChanged.connect(self.move_baseline)
        self.baseline_adjust_slider.valueChanged.connect(self.enable_save_config_button)
        # Define system_export_path_tool_button
        self.system_export_path_tool_button = system_export_path_tool_button
        self.system_export_path_tool_button.clicked.connect(self.select_system_export_path)
        # Define tabWidget change tab
        self.tabWidget.currentChanged.connect(self.allow_change_tab)
        self.tabWidget.tabBarClicked.connect(self.tab_check_and_reset_tab)
        # Define save_config_button
        self.save_config_button = save_config_button
        self.save_config_button.clicked.connect(self.trigger_all_update)
        # Define system_export_path_edit
        self.system_export_path_edit = system_export_path_edit
        self.system_export_path_edit.textChanged.connect(self.enable_save_config_button)
        self.old_system_export_path = ""
        # Define upload_url_line_edit
        self.upload_url_line_edit = upload_url_line_edit
        self.upload_url_line_edit.textChanged.connect(self.enable_save_config_button)
        # Define upload_test_button
        self.upload_test_button = upload_test_button
        self.upload_test_button.clicked.connect(self.test_upload_api)
        # Define camera thread
        self.camera_thread = VideoThread()
        self.camera_thread.change_pixmap_signal.connect(self.update_image)

    def add_filed_line_component(self, field_label: QLabel = None, line_edit: QLineEdit = None,
                                 lock_button: QPushButton = None, rename_line_edit: QLineEdit = None,
                                 used_box: QCheckBox = None, field_group: QWidget = None):
        if (field_label is None) or (line_edit is None) or (lock_button is None) \
                or (used_box is None) or (field_group is None) or (rename_line_edit is None):
            return
        state = True
        if used_box.isChecked():
            state = False
        self.field_group_list.append(FieldComponent(state, field_label, line_edit, lock_button,
                                                    rename_line_edit, used_box, field_group))
        lock_button.clicked.connect(self.set_all_edit_enable)
        used_box.clicked.connect(self.enable_save_config_button)
        rename_line_edit.textChanged.connect(self.enable_save_config_button)
        return self

    def trigger_baseline_adjust_slider(self, type):
        currentValue = self.baseline_adjust_slider.value()
        if type == 'up':
            if currentValue < self.baseline_adjust_slider.maximum():
                self.baseline_adjust_slider.setValue(currentValue + 1)
        if type == 'down':
            if currentValue > self.baseline_adjust_slider.minimum():
                self.baseline_adjust_slider.setValue(currentValue - 1)

    def move_baseline(self):
        self.baseline_ratio = self.baseline_adjust_slider.value() / self.baseline_adjust_slider.maximum()
        new_y = self.baseline_y0 - self.baseline_adjust_slider.value()
        qRect = self.baseline_adjust.geometry()
        self.baseline_adjust.setGeometry(QRect(qRect.x(), new_y, qRect.width(), qRect.height()))

    def select_system_export_path(self):
        dialog = QFileDialog()
        folder_path = dialog.getExistingDirectory(None, "Select Folder")
        if folder_path != "":
            self.system_export_path_edit.setText(folder_path)

    def allow_change_tab(self):
        if self.tabWidget.currentIndex() != 1 and self.return_config_tab:
            self.tabWidget.setCurrentIndex(1)

    def tab_check_and_reset_tab(self, index):
        if index != 1 and self.tabWidget.currentIndex() == 1:
            if self.save_config_button.isEnabled():
                _decision_dialog = decision_dialog.DecisionDialog()
                _decision_dialog.ui.message.setText('未儲存設定，是否確定離開？')
                if not _decision_dialog.exec_():
                    self.return_config_tab = True
                else:
                    self.camera_thread.stop()
                    self.return_config_tab = False
                    self.save_config_button.setEnabled(False)

            else:
                self.camera_thread.stop()
        elif index == 1 and self.tabWidget.currentIndex() == 1:
            pass
        elif index == 1:
            self.setup_system()

    def test_upload_api(self):
        url = self.upload_url_line_edit.text()
        _message_dialog = message_dialog.CustomDialog()
        csv_file = {'upload_file': open('./test.csv', 'rb')}
        try:
            # response = requests.get(url)
            response = requests.post(url, files=csv_file)
            if response.status_code == 200 and response.text == 'OK':
                _message_dialog.ui.message.setText("測試成功")
                _message_dialog.setWindowTitle("測試成功")
            else:
                _message_dialog.ui.message.setText("失敗！（未接收到'OK'字串回傳或Http 200 State）")
                _message_dialog.setWindowTitle("錯誤")
                _message_dialog.ui.buttonBox.setStandardButtons(QDialogButtonBox.Close)
        except:
            _message_dialog.ui.message.setText("無法辨識之URL")
            _message_dialog.setWindowTitle("錯誤")
            _message_dialog.ui.buttonBox.setStandardButtons(QDialogButtonBox.Close)
        _message_dialog.exec()

    def save_config(self):
        Config = Query()
        # Save field config
        for index, fieldComponent in enumerate(self.field_group_list):
            db.config_table.upsert(FieldComponentValue(index, fieldComponent).__dict__, Config.index == index)

        # Update save folder path in detection tab
        if ManagerHub.table_manager is not None:
            table_manager = ManagerHub.table_manager
            if self.old_system_export_path in table_manager.save_folder_path:
                table_manager.save_folder_path = os.path.join(self.system_export_path_edit.text(),
                                                              '') + table_manager.save_folder_line_edit.text()
                self.old_system_export_path = self.system_export_path_edit.text()
        # Update table column in detection tab
        # if ManagerHub.table_manager is not None:
        #     ManagerHub.table_manager.init_table()
        # Update threshold in detection tab
        if ManagerHub.detection_manager is not None:
            ManagerHub.detection_manager.reset_baseline_value()

        # Save system_export_path config
        system_export_path_val = {'index': 4, 'path': self.system_export_path_edit.text()}
        db.config_table.upsert(system_export_path_val, Config.index == 4)
        # Save upload_url_db config
        upload_url_db_val = {'index': 5, 'url': self.upload_url_line_edit.text()}
        db.config_table.upsert(upload_url_db_val, Config.index == 5)
        # Save threshold config
        threshold_val = {'index': 6, 'value': self.baseline_adjust_slider.value()}
        db.config_table.upsert(threshold_val, Config.index == 6)
        will_save_process_image = {'index': 7, 'value': self.save_process_image.isChecked()}
        db.config_table.upsert(will_save_process_image, Config.index == 7)

    def disable_save_config_button(self):
        if self.save_config_button.isEnabled():
            self.save_config()
            self.save_config_button.setEnabled(False)
            self.return_config_tab = False

    def enable_save_config_button(self):
        self.save_config_button.setEnabled(True)

    def set_all_edit_enable(self):
        for fieldComponent in self.field_group_list:
            if fieldComponent.lock_button.isChecked():
                fieldComponent.line_edit.setEnabled(False)
            else:
                fieldComponent.line_edit.setEnabled(True)

    def set_all_group_visible(self):
        for fieldComponent in self.field_group_list:
            if fieldComponent.used_box.isChecked():
                fieldComponent.state = True
                fieldComponent.field_group.setVisible(True)
            else:
                fieldComponent.state = False
                fieldComponent.field_group.setVisible(False)

    def set_all_field_label_text(self):
        for fieldComponent in self.field_group_list:
            fieldComponent.field_label.setText(fieldComponent.rename_line_edit.text())

    def trigger_all_update(self):
        self.set_all_group_visible()
        self.set_all_field_label_text()
        self.disable_save_config_button()

    def get_next_enable_line_edit(self, index):
        for i in range(index, len(self.field_group_list)):
            fieldComponent: FieldComponent = self.field_group_list[i]
            if i > index and fieldComponent.line_edit.isEnabled():
                index = i
                break
        return index

    def change_focus_line_edit(self, currentIndex):
        next_index = self.get_next_enable_line_edit(currentIndex)
        fieldComponent: FieldComponent = self.field_group_list[next_index]
        fieldComponent.line_edit.setFocus()

    def setup_system(self):
        configs = db.config_table.all()
        for config in configs:
            if config['index'] == 4:
                self.system_export_path_edit.setText(config['path'])
                self.system_export_path_edit.setCursorPosition(0)
                self.old_system_export_path = config['path']
            elif config['index'] == 5:
                self.upload_url_line_edit.setText(config['url'])
            elif config['index'] == 6:
                self.baseline_adjust_slider.setValue(config['value'])
            elif config['index'] == 7:
                if self.account.username != "system":
                    self.save_process_image.setChecked(False)
                else:
                    self.save_process_image.setChecked(config['value'])
            else:
                fieldComponent: FieldComponent = self.field_group_list[config['index']]
                fieldComponent.state = config['state']
                fieldComponent.field_label.setText(config['field_label_text'])
                fieldComponent.rename_line_edit.setText(config['field_label_text'])
                fieldComponent.used_box.setChecked(config['used_box'])
        for index, _fieldComponent in enumerate(self.field_group_list):
            fieldComponent: FieldComponent = _fieldComponent
            if index < len(self.field_group_list) - 1:
                if index == 0:
                    fieldComponent.line_edit.returnPressed.connect(lambda: self.change_focus_line_edit(0))
                elif index == 1:
                    fieldComponent.line_edit.returnPressed.connect(lambda: self.change_focus_line_edit(1))
                elif index == 2:
                    fieldComponent.line_edit.returnPressed.connect(lambda: self.change_focus_line_edit(2))
        self.baseline_use_image.setPixmap(QtGui.QPixmap("./images/default_image.jpg"))
        self.trigger_all_update()

    def trigger_camera_activity(self, p1):
        self.camera_thread.start()

    def update_image(self, cv_img):
        qt_img = self.convert_cv_qt(cv_img)
        self.baseline_use_image.setPixmap(qt_img)

    def convert_cv_qt(self, cv_img):
        """Convert from an opencv image to QPixmap"""
        rgb_image = cv2.cvtColor(cv_img, cv2.COLOR_BGR2RGB)
        h, w, ch = rgb_image.shape
        bytes_per_line = ch * w
        convert_to_Qt_format = QtGui.QImage(rgb_image.data, w, h, bytes_per_line, QtGui.QImage.Format_RGB888)
        p = convert_to_Qt_format.scaled(321, 511, Qt.KeepAspectRatio)
        return QPixmap.fromImage(p)
