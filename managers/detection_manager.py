﻿# -*- coding:utf-8 -*-
import os
import time
import json
import cv2
from pypylon import pylon
import numpy as np
from detection_tool.Pcb_Project._init_paths import *
from Controller.DetectNGController import DetectNGController
from Controller.PcbCounterController import PcbCounterController
from PyQt5 import QtGui
from PyQt5.QtCore import QRect, QThread, pyqtSignal, Qt
from PyQt5.QtGui import QPixmap
from PyQt5.QtWidgets import QLabel, QFrame
from managers.manager_hub import ManagerHub
from managers.camera_manager import *
from Controller.ImageSystemController import ImageSystemController

class PCBCounterThread(QThread):
    change_pcb_amount_signal = pyqtSignal(dict)

    def __init__(self):
        super().__init__()
        self.current_image = None
        self.record_image_dir = ""

    def run(self):
        config_path = './detection_tool/Pcb_Project/config/pcb.ini'
        pcbCounterController = PcbCounterController()
        pcbCounterController.setConfigPath(config_path)
        try:
            savePath = ""
            if ManagerHub.config_manager.save_process_image.isChecked():
                if not os.path.exists(self.record_image_dir):
                    os.makedirs(self.record_image_dir)
                savePath = self.record_image_dir
            pcbCounterController.setGapSwitch("Turn on")
            #imageSystemController.setImageByPath("./images/image.jpg")            
            # result = pcbCounterController.run(imageSystemController.getImage())
            dtime = time.time()
            result = pcbCounterController.run(self.current_image, savePath)
            print('*pcbcount: ' , time.time()-dtime)
            # result = pcbCounterController.run(self.current_image)
            self.change_pcb_amount_signal.emit(result)
        except Exception as e:
            print(e)
            result = {'result(pcs)': "NG", 'label': "NG"}
            self.change_pcb_amount_signal.emit(result)


class DetectionNGThread(QThread):
    change_state_signal = pyqtSignal(str)

    def __init__(self, baseline_ratio):
        super().__init__()
        self.baseline_ratio = baseline_ratio
        self.current_image = None

    def run(self):
        if self.current_image is None:
            return
        detectNGController = DetectNGController()
        #            if self.image_pipeline is not None:
        try:
            detectNGController.detectFailService.setConfiguration(baselineThreshold=self.baseline_ratio)
            result = detectNGController.detectNG(self.current_image)
            #print(result)
            self.change_state_signal.emit(result ['state'])
        except Exception as e:
            print(e)
            self.change_state_signal.emit("NG")


class VideoThread(QThread):
    change_pixmap_signal = pyqtSignal(np.ndarray)

    def __init__(self):
        super().__init__()

    # def run(self):
    #    # img = cv2.imread('./detection_tool/Pcb_Project/demo/dataset/test.jpg')
    #    img = cv2.imread('./detection_tool/Pcb_Project/demo/1.jpg')
    #    cv_img = cv2.rotate(img, cv2.cv2.ROTATE_90_CLOCKWISE)
    #    # cv_img = self.rotate(img, 270)
    #    self.change_pixmap_signal.emit(cv_img)

    def run(self):
        if Camera.IsGrabbing():
            dtime = time.time()
            grabResult = Camera.RetrieveResult(5000, pylon.TimeoutHandling_ThrowException)
            print('*grabResult: ', time.time()-dtime)
            dtime = time.time()
            if grabResult.GrabSucceeded():
                img = grabResult.Array
                img = cv2.rotate(img, cv2.cv2.ROTATE_90_CLOCKWISE)
                print('*rotate: ' ,time.time()-dtime)
                self.change_pixmap_signal.emit(img)
            assert grabResult.GrabSucceeded() == True
            grabResult.Release()



class DetectionManager:
    def __init__(self, detection_monitor: QLabel, detection_baseline: QFrame, amount_label: QLabel,
                 detect_state_label: QLabel, ):
        self.detection_monitor = detection_monitor
        self.detection_baseline = detection_baseline
        self.baseline_y0 = self.detection_monitor.height() - 2
        self.baseline_ratio = ManagerHub.config_manager.baseline_ratio
        self.amount_label = amount_label
        self.detect_state_label = detect_state_label
        self.current_image = None
        self.current_image_encode = None
        # Define camera thread
        self.camera_thread = VideoThread()
        self.camera_thread.change_pixmap_signal.connect(self.update_image)
        # Define detectionNG thread
        #self.detection_ng_thread = DetectionNGThread(self.baseline_ratio)  # {"state": Normal/NG}
        #self.detection_ng_thread.change_state_signal.connect(self.update_ng_state)
        # Define PCB Counter thread
        self.pcb_counter_thread = PCBCounterThread()  # {'usedTime(s)': end-start, 'result(pcs)': count}
        self.pcb_counter_thread.change_pcb_amount_signal.connect(self.update_detection_data)
        self.reset_baseline_value()
        self.recodeFileName = None
        self.debug_time = None

    def reset_baseline_value(self):
        self.baseline_ratio = ManagerHub.config_manager.baseline_ratio
        new_y = self.baseline_y0 - (self.detection_monitor.height() * self.baseline_ratio)
        qRect = self.detection_baseline.geometry()
        self.detection_baseline.setGeometry(QRect(qRect.x(), new_y, qRect.width(), qRect.height()))
        #if self.detection_ng_thread is not None:
        #    self.detection_ng_thread.baseline_ratio = self.baseline_ratio

    def trigger_detection(self):
        self.debug_time = time.time()
        self.recodeFileName = str(int(time.time()))
        record_folder = os.path.join(ManagerHub.table_manager.save_folder_path, '') + self.recodeFileName
        self.pcb_counter_thread.record_image_dir = record_folder
        self.detect_state_label.setText("拍照中")
        self.amount_label.setText("---")
        self.camera_thread.start()

    def update_detection_data(self, data):
        ManagerHub.table_manager.record_button.setEnabled(True)
        #print(data)
        if data['label'] not in ["panel", "ambiguous"]:
            self.detect_state_label.setText("辨識完畢")
            self.amount_label.setText(str(data['result(pcs)']))
            dtime = time.time()
            ManagerHub.table_manager.record_data(self.recodeFileName)
            print('*record: ', time.time()-dtime)
        else:
            if data['label'] == "panel":
                self.detect_state_label.setText("辨識完畢(板面露出)")
            else:
                self.detect_state_label.setText("辨識完畢(照片模糊)")
            self.amount_label.setText("NG")
            if ManagerHub.config_manager.save_process_image.isChecked():
                ManagerHub.table_manager.record_data(self.recodeFileName)
        print('Process time: ', time.time() - self.debug_time)
        print('-------------------------')

    def update_ng_state(self, state):
        #if state == "NG":
        #    ManagerHub.table_manager.record_button.setEnabled(True)
        #    self.detect_state_label.setText("辨識完畢")
        #    self.amount_label.setText("NG")
        #else:
        self.pcb_counter_thread.start()

    def update_image(self, cv_img):
        """Updates the image_label with a new opencv image"""
        qt_img = self.convert_cv_qt(cv_img)
        self.detection_monitor.setPixmap(qt_img)
        #self.detection_ng_thread.start()
        self.pcb_counter_thread.start()
        self.detect_state_label.setText("辨識中")

    def convert_cv_qt(self, cv_img):
        """Convert from an opencv image to QPixmap"""
        dtime = time.time()
        brg_img = cv2.cvtColor(cv_img, cv2.COLOR_GRAY2BGR)
        print(brg_img.shape)
        print('*cvtColor: ' ,time.time()-dtime)
        dtime = time.time()
        self.current_image_encode = cv2.imencode('.jpg', cv2.resize(brg_img, (int(brg_img.shape[1]/3), int(brg_img.shape[0]/3)), interpolation = cv2.INTER_AREA))[1]
        print('*imencode: ' ,time.time()-dtime)
        #npimg = np.fromstring(self.current_image_encode.tostring(), dtype=np.uint8)
        #brg_img = cv2.imdecode(npimg, -1)
        self.current_image = brg_img
        #self.detection_ng_thread.current_image = brg_img
        self.pcb_counter_thread.current_image = brg_img
        h, w, ch = brg_img.shape
        bytes_per_line = ch * w
        convert_to_Qt_format = QtGui.QImage(brg_img.data, w, h, bytes_per_line, QtGui.QImage.Format_RGB888)
        p = convert_to_Qt_format.scaled(218, 328, Qt.KeepAspectRatio)
        return QPixmap.fromImage(p)
