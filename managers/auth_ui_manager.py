import statistics

from PyQt5.QtWidgets import QCheckBox, QWidget

from domain.account import Account


class AuthUIManager:
    upload_checkbox: QCheckBox = None
    threshold_adjust: QWidget = None
    field_setting_group: QWidget = None
    export_path_setting_block: QWidget = None
    upload_url_setting_block: QWidget = None

    @classmethod
    def set_component_instance(cls, upload_checkbox: QCheckBox, threshold_adjust: QWidget, field_setting_group: QWidget,
                               export_path_setting_block: QWidget, upload_url_setting_block: QWidget,
                               detect_option_group: QWidget,):
        cls.upload_checkbox = upload_checkbox
        cls.threshold_adjust = threshold_adjust
        cls.field_setting_group = field_setting_group
        cls.export_path_setting_block = export_path_setting_block
        cls.upload_url_setting_block = upload_url_setting_block
        cls.detect_option_group = detect_option_group

    @classmethod
    def render_ui_by_account_setting(cls, account: Account):
        if not account.can_edit_field_name:
            cls.field_setting_group.setVisible(False)
        else:
            cls.field_setting_group.setVisible(True)

        if not account.can_edit_ng_threshold:
            cls.threshold_adjust.setVisible(False)
        else:
            cls.threshold_adjust.setVisible(True)

        if not account.can_edit_export_path:
            cls.export_path_setting_block.setVisible(False)
        else:
            cls.export_path_setting_block.setVisible(True)

        if not account.can_edit_upload_url:
            cls.upload_url_setting_block.setVisible(False)
        else:
            cls.upload_url_setting_block.setVisible(True)

        if not account.can_upload_csv:
            cls.upload_checkbox.setVisible(False)
        else:
            cls.upload_checkbox.setVisible(True)

        if account.username != "system":
            cls.detect_option_group.setVisible(False)
        else:
            cls.detect_option_group.setVisible(True)

