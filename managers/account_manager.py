﻿from PyQt5.QtWidgets import QTableWidget, QLineEdit, QPushButton, QMainWindow, QHeaderView, QCheckBox, QTableWidgetItem, \
    QWidget, QDialogButtonBox
from tinydb import Query

from agents.db_agent import DBAgent
from PyQt5.QtCore import Qt
from domain.account import Account
from enum import Enum
import message_dialog
import decision_dialog
from managers.auth_ui_manager import AuthUIManager


class EditMode(Enum):
    EDIT = 0
    CREATE = 1


class AccountManager:
    def __init__(self,
                 current_account: Account,
                 account_table: QTableWidget,
                 user_name_line_edit: QLineEdit,
                 job_number_line_edit: QLineEdit,
                 password_line_edit: QLineEdit,
                 cancel_button: QPushButton,
                 save_button: QPushButton,
                 auth_edit_block: QWidget,
                 can_edit_field_name: QCheckBox,
                 can_edit_ng_threshold: QCheckBox,
                 can_edit_export_path: QCheckBox,
                 can_edit_upload_url: QCheckBox,
                 can_upload_csv: QCheckBox,
                 can_edit_account: QCheckBox):
        self.edit_mode = EditMode.CREATE  # update_account / new_account
        self.current_edit_row = -1
        self.current_account = current_account
        self.save_button = save_button
        self.save_button.clicked.connect(self.upsert_account)
        self.cancel_button = cancel_button
        self.cancel_button.clicked.connect(self.init_edit_component)
        self.password_line_edit = password_line_edit
        self.job_number_line_edit = job_number_line_edit
        self.user_name_line_edit = user_name_line_edit
        self.account_table = account_table
        self.auth_edit_block = auth_edit_block
        self.can_edit_field_name = can_edit_field_name
        self.can_edit_ng_threshold = can_edit_ng_threshold
        self.can_edit_export_path = can_edit_export_path
        self.can_edit_upload_url = can_edit_upload_url
        self.can_upload_csv = can_upload_csv
        self.can_edit_account = can_edit_account
        self.init_table_header()
        self.init_table_data()
        self.init_edit_component()
        # self.current_edit_row = self.find_current_account_row()

    def init_edit_component(self):
        self.user_name_line_edit.clear()
        self.user_name_line_edit.setReadOnly(False)
        self.job_number_line_edit.clear()
        self.password_line_edit.clear()
        self.can_edit_field_name.setChecked(False)
        self.can_edit_ng_threshold.setChecked(False)
        self.can_edit_export_path.setChecked(False)
        self.can_edit_upload_url.setChecked(False)
        self.can_upload_csv.setChecked(False)
        self.can_edit_account.setChecked(False)
        self.can_edit_account.setVisible(True)
        self.current_edit_row = self.find_current_account_row()
        if not self.current_account.can_edit_account:
            self.edit_mode = EditMode.EDIT
            self.user_name_line_edit.setText(self.current_account.username)
            self.user_name_line_edit.setReadOnly(True)
            self.job_number_line_edit.setText(self.current_account.job_number)
            self.password_line_edit.setText(self.current_account.password)
            self.save_button.setText("儲存")
            self.auth_edit_block.setVisible(False)
            self.can_edit_field_name.setChecked(self.current_account.can_edit_field_name)
            self.can_edit_ng_threshold.setChecked(self.current_account.can_edit_ng_threshold)
            self.can_edit_export_path.setChecked(self.current_account.can_edit_export_path)
            self.can_edit_upload_url.setChecked(self.current_account.can_edit_upload_url)
            self.can_upload_csv.setChecked(self.current_account.can_upload_csv)
            self.can_edit_account.setChecked(self.current_account.can_edit_account)
        else:
            self.edit_mode = EditMode.CREATE
            self.save_button.setText("新增帳戶")
            self.auth_edit_block.setVisible(True)

    def find_account_row_by_username(self, username):
        for i in range(self.account_table.rowCount()):
            if self.account_table.item(i, 2).text() == username:
                return i
        return -1

    def find_current_account_row(self):
        for i in range(self.account_table.rowCount()):
            if self.account_table.item(i, 2).text() == self.current_account.username:
                return i
        return -1

    def upsert_account(self):
        if not self.job_number_line_edit.text() or not self.user_name_line_edit.text() or not self.password_line_edit.text():
            _message_dialog = message_dialog.CustomDialog()
            _message_dialog.ui.message.setText("請確認資料填寫完整")
            _message_dialog.setWindowTitle("錯誤")
            _message_dialog.ui.buttonBox.setStandardButtons(QDialogButtonBox.Close)
            _message_dialog.exec()
            return
        username = self.user_name_line_edit.text()
        job_number = self.job_number_line_edit.text()
        password = self.password_line_edit.text()
        AccountQuery = Query()
        if self.edit_mode == EditMode.CREATE:
            account = DBAgent.account_table.search(AccountQuery.username == username)
            if len(account) > 0:
                _message_dialog = message_dialog.CustomDialog()
                _message_dialog.ui.message.setText("已存在相同帳號!")
                _message_dialog.setWindowTitle("錯誤")
                _message_dialog.ui.buttonBox.setStandardButtons(QDialogButtonBox.Close)
                _message_dialog.exec()
                return
            account = DBAgent.account_table.search(AccountQuery.job_number == job_number)
            if len(account) > 0:
                _message_dialog = message_dialog.CustomDialog()
                _message_dialog.ui.message.setText("已存在相同員工編號!")
                _message_dialog.setWindowTitle("錯誤")
                _message_dialog.ui.buttonBox.setStandardButtons(QDialogButtonBox.Close)
                _message_dialog.exec()
                return
        else:
            self.current_edit_row = self.find_account_row_by_username(username)
            account = DBAgent.account_table.search(AccountQuery.job_number == job_number)
            if len(account) > 0 and self.account_table.item(self.current_edit_row, 3).text() != job_number:
                _message_dialog = message_dialog.CustomDialog()
                _message_dialog.ui.message.setText("已存在相同員工編號!")
                _message_dialog.setWindowTitle("錯誤")
                _message_dialog.ui.buttonBox.setStandardButtons(QDialogButtonBox.Close)
                _message_dialog.exec()
                return

        account = Account(username=username, job_number=job_number, password=password,
                          can_edit_field_name=self.can_edit_field_name.isChecked(),
                          can_edit_ng_threshold=self.can_edit_ng_threshold.isChecked(),
                          can_edit_export_path=self.can_edit_export_path.isChecked(),
                          can_edit_upload_url=self.can_edit_upload_url.isChecked(),
                          can_upload_csv=self.can_upload_csv.isChecked(),
                          can_edit_account=self.can_edit_account.isChecked())
        DBAgent.account_table.upsert(account.__dict__, AccountQuery.username == account.username)
        if self.edit_mode == EditMode.CREATE:
            self.addTableRow(account.__dict__)
        else:
            self.updateTableRow(account.__dict__)
        if account.username == self.current_account.username:
            self.current_account = account
            AuthUIManager.render_ui_by_account_setting(account)

    def init_table_header(self):
        columnNum = 11
        columnNameList = ["delete", "update", "username", "job_Number", "password", "can_edit_field_name",
                          "can_edit_ng_threshold", "can_edit_export_path", "can_edit_upload_url", "can_upload_csv",
                          "can_edit_account"]
        self.account_table.setColumnCount(columnNum)
        self.account_table.rowCount()
        self.account_table.setHorizontalHeaderLabels(columnNameList)
        for i in range(self.account_table.columnCount()):
            self.account_table.horizontalHeader().setSectionResizeMode(i, QHeaderView.ResizeToContents)

    def handleDeleteClicked(self, button):
        # or button = self.sender()
        index = self.account_table.indexAt(button.pos())
        username = self.account_table.item(index.row(), 2).text()
        _decision_dialog = decision_dialog.DecisionDialog()
        _decision_dialog.ui.message.setText('確定將帳號 %s 刪除？' % username)
        if _decision_dialog.exec_():
            AccountQuery = Query()
            DBAgent.account_table.remove(AccountQuery.username == username)
            self.account_table.removeRow(index.row())
            self.current_edit_row = self.find_current_account_row()

    def handleEditClicked(self, button):
        # or button = self.sender()
        index = self.account_table.indexAt(button.pos())
        if index.isValid():
            self.current_edit_row = index.row()
            self.edit_mode = EditMode.EDIT
            self.user_name_line_edit.setReadOnly(True)
            self.user_name_line_edit.setText(self.account_table.item(index.row(), 2).text())
            self.job_number_line_edit.setText(self.account_table.item(index.row(), 3).text())
            self.password_line_edit.setText(self.account_table.cellWidget(index.row(), 4).text())
            self.can_edit_field_name.setChecked(self.account_table.item(index.row(), 5).text() == "True")
            self.can_edit_ng_threshold.setChecked(self.account_table.item(index.row(), 6).text() == "True")
            self.can_edit_export_path.setChecked(self.account_table.item(index.row(), 7).text() == "True")
            self.can_edit_upload_url.setChecked(self.account_table.item(index.row(), 8).text() == "True")
            self.can_upload_csv.setChecked(self.account_table.item(index.row(), 9).text() == "True")
            self.can_edit_account.setChecked(self.account_table.item(index.row(), 10).text() == "True")
            if self.account_table.item(index.row(), 2).text() == self.current_account.username:
                self.can_edit_account.setVisible(False)
            else:
                self.can_edit_account.setVisible(True)
            self.save_button.setText("儲存")

    def addTableRow(self, account=None):
        if account['username'] == 'system':
            return
        data = [account['username'], account['job_number'], account['password'], account['can_edit_field_name'],
                account['can_edit_ng_threshold'], account['can_edit_export_path'], account['can_edit_upload_url'],
                account['can_upload_csv'], account['can_edit_account']]
        row = self.account_table.rowCount()
        self.account_table.setRowCount(row + 1)
        edit_btn_cell = QPushButton('edit')
        if not(self.current_account.can_edit_account or data[0] == self.current_account.username):
            edit_btn_cell.setEnabled(False)
        edit_btn_cell.clicked.connect(lambda: self.handleEditClicked(edit_btn_cell))
        self.account_table.setCellWidget(row, 1, edit_btn_cell)

        del_btn_cell = QPushButton('Delete')
        if not (self.current_account.can_edit_account and data[0] != self.current_account.username):
            del_btn_cell.setEnabled(False)
        del_btn_cell.clicked.connect(lambda: self.handleDeleteClicked(del_btn_cell))
        self.account_table.setCellWidget(row, 0, del_btn_cell)

        for index, item in enumerate(data):
            _index = index + 2
            if _index == 4:
                pass_word_line_edit = QLineEdit()
                pass_word_line_edit.setReadOnly(True)
                pass_word_line_edit.setText(item)
                pass_word_line_edit.setEchoMode(QLineEdit.Password)
                self.account_table.setCellWidget(row, _index, pass_word_line_edit)
            else:
                cell = QTableWidgetItem(str(item))
                cell.setFlags(Qt.ItemIsEnabled)
                self.account_table.setItem(row, _index, cell)

    def init_table_data(self):
        accounts = DBAgent.account_table.all()
        for account in accounts:
            self.addTableRow(account)

    def updateTableRow(self, account):
        data = [account['username'], account['job_number'], account['password'], account['can_edit_field_name'],
                account['can_edit_ng_threshold'], account['can_edit_export_path'], account['can_edit_upload_url'],
                account['can_upload_csv'],
                account['can_edit_account']]
        row = self.current_edit_row
        for index, item in enumerate(data):
            _index = index + 2
            if _index == 4:
                pass_word_line_edit = QLineEdit()
                pass_word_line_edit.setReadOnly(True)
                pass_word_line_edit.setText(item)
                pass_word_line_edit.setEchoMode(QLineEdit.Password)
                self.account_table.setCellWidget(row, _index, pass_word_line_edit)
            else:
                cell = QTableWidgetItem(str(item))
                cell.setFlags(Qt.ItemIsEnabled)
                self.account_table.setItem(row, _index, cell)
