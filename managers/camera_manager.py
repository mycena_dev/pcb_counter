# -*- coding:utf-8 -*-
from pypylon import pylon

Camera = pylon.InstantCamera(pylon.TlFactory.GetInstance().CreateFirstDevice())
Camera.Open()
new_width = Camera.Width.GetValue() - Camera.Width.GetInc()
if new_width >= Camera.Width.GetMin():
    Camera.Width.SetValue(new_width)
Camera.StartGrabbing(pylon.GrabStrategy_LatestImages)
pylon.FeaturePersistence.Load("./images/config2.txt", Camera.GetNodeMap())


# Camera = None
