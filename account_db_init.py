from enum import Enum

from tinydb import Query

from agents.db_agent import DBAgent as db
from domain.account import Account

system_account = Account('system', 'system', 'system', True, True, True, True, True, True)
administrator_account = Account('administrator', 'administrator', '00001', True, True, True, True, True, True)
operator_account = Account('operator', 'operator', '10031', False, True, True, True, True, True)
account = Query()
db.account_table.upsert(system_account.__dict__, account.username == system_account.username)
db.account_table.upsert(administrator_account.__dict__, account.username == administrator_account.username)
db.account_table.upsert(operator_account.__dict__, account.username == operator_account.username)
print(db.account_table.all())
account = db.account_table.search(account.username == "asd")
print(len(account))