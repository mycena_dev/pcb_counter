from PyQt5.QtWidgets import QDialog
import uis.alerts


class CustomDialog(QDialog):
    def __init__(self, parent=None):
        super().__init__(parent)
        self.ui = uis.alerts.Ui_Dialog()
        self.ui.setupUi(self)
