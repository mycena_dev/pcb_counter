from PyQt5.QtWidgets import QLineEdit


class MyQLineEdit(QLineEdit):
    def focusInEvent(self, e):
        # Do something with the event here
        self.clear()
        super(MyQLineEdit, self).focusInEvent(e)