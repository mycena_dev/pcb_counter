from PyQt5 import QtGui, QtWidgets
import sys
from login_window import LoginWindow
from main_window import MainWindow


def login():
    dialog = LoginWindow()
    if dialog.exec_():
        return True, dialog.account
    else:
        return False, None


if __name__ == '__main__':
    app = QtWidgets.QApplication([])
    state, account = login()
    if state:
        window = MainWindow(account)
        window.show()
        # window.showMaximized()
        sys.exit(app.exec_())
