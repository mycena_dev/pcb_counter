from PyQt5 import QtWidgets

from domain.account import Account
from managers import config_manager, table_manager, detection_manager
from managers.account_manager import AccountManager
from managers.manager_hub import ManagerHub
from uis.main_window import Ui_MainWindow
from managers.auth_ui_manager import AuthUIManager

class MainWindow(QtWidgets.QMainWindow):
    def __init__(self, account: Account,):
        super(MainWindow, self).__init__()
        self.ui = Ui_MainWindow()
        self.ui.setupUi(self)

        AuthUIManager.set_component_instance(upload_checkbox=self.ui.upload_checkbox,
                                             threshold_adjust=self.ui.threshold_adjust,
                                             field_setting_group=self.ui.field_setting_group,
                                             export_path_setting_block=self.ui.export_path_setting_block,
                                             upload_url_setting_block=self.ui.upload_url_setting_block,
                                             detect_option_group=self.ui.detect_option_group)
        AuthUIManager.render_ui_by_account_setting(account)


        self.ui.account_number_label.setText(account.job_number)

        self.configManager = config_manager.ConfigManager(tabWidget=self.ui.tabWidget,
                                                          save_config_button=self.ui.save_config_button,
                                                          system_export_path_tool_button=self.ui.system_export_path_tool_button,
                                                          system_export_path_edit=self.ui.system_export_path_line_edit,
                                                          upload_url_line_edit=self.ui.upload_api_line_edit,
                                                          upload_test_button=self.ui.upload_test_button,
                                                          baseline_adjust=self.ui.baseline_adjust,
                                                          baseline_adjust_down=self.ui.baseline_adjust_down,
                                                          baseline_adjust_up=self.ui.baseline_adjust_up,
                                                          baseline_adjust_slider=self.ui.baseline_adjust_slider,
                                                          baseline_use_image=self.ui.baseline_use_image,
                                                          save_process_image=self.ui.save_process_image,
                                                          account=account) \
            .add_filed_line_component(self.ui.field1_label, self.ui.field1_line_edit,
                                      self.ui.field1_lock_button, self.ui.field1_customized_line_edit,
                                      self.ui.field1_used_check_box, self.ui.field1_group) \
            .add_filed_line_component(self.ui.field2_label, self.ui.field2_line_edit,
                                      self.ui.field2_lock_button, self.ui.field2_customized_line_edit,
                                      self.ui.field2_used_check_box, self.ui.field2_group) \
            .add_filed_line_component(self.ui.field3_label, self.ui.field3_line_edit,
                                      self.ui.field3_lock_button, self.ui.field3_customized_line_edit,
                                      self.ui.field3_used_check_box, self.ui.field3_group) \
            .add_filed_line_component(self.ui.field4_label, self.ui.field4_line_edit,
                                      self.ui.field4_lock_button, self.ui.field4_customized_line_edit,
                                      self.ui.field4_used_check_box, self.ui.field4_group)
        self.configManager.setup_system()
        ManagerHub.config_manager = self.configManager
        self.tableManager = table_manager.TableManager(job_num=self.ui.account_number_label.text(),
                                                       save_folder_line_edit=self.ui.save_folder_line_edit,
                                                       save_folder_tool_button=self.ui.save_folder_tool_button,
                                                       qtable=self.ui.record_table, record_button=self.ui.record_button,
                                                       pcb_number_label=self.ui.pcb_number_label,
                                                       upload_checkbox=self.ui.upload_checkbox,
                                                       update_recode_button=self.ui.update_recode_button)
        ManagerHub.table_manager = self.tableManager
        self.detectionManager = detection_manager.DetectionManager(detection_monitor=self.ui.image_label,
                                                                   detection_baseline=self.ui.baseline,
                                                                   amount_label=self.ui.pcb_number_label,
                                                                   detect_state_label=self.ui.detect_state_label)
        ManagerHub.detection_manager = self.detectionManager

        self.accountManager = AccountManager(current_account=account,
                                             account_table=self.ui.account_table,
                                             user_name_line_edit=self.ui.user_name_line_edit,
                                             job_number_line_edit=self.ui.job_number_line_edit,
                                             password_line_edit=self.ui.password_line_edit,
                                             cancel_button=self.ui.cancel_button,
                                             save_button=self.ui.save_account_button,
                                             auth_edit_block=self.ui.auth_setting_block,
                                             can_edit_field_name=self.ui.can_edit_field_name,
                                             can_edit_ng_threshold=self.ui.can_edit_ng_threshold,
                                             can_edit_export_path=self.ui.can_edit_export_path,
                                             can_edit_upload_url=self.ui.can_edit_upload_url,
                                             can_upload_csv=self.ui.can_upload_csv,
                                             can_edit_account=self.ui.can_edit_account)


