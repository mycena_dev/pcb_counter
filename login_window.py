from PyQt5.QtWidgets import QDialog
from tinydb import Query

from agents.db_agent import DBAgent
from domain.account import Account
from uis.login_dialog import Ui_Dialog
import message_dialog


class LoginWindow(QDialog):
    def __init__(self):
        super(LoginWindow, self).__init__()
        self.account = None
        self.ui = Ui_Dialog()
        self.ui.setupUi(self)
        self.ui.loginButton.clicked.connect(lambda: self.login())

    def login(self):
        user_name = self.ui.userName_edit.text()
        password = self.ui.password_edit.text()
        AccountQuery = Query()
        account = DBAgent.account_table.search(AccountQuery.username == user_name)
        dlg = message_dialog.CustomDialog()
        if len(account) < 1:
            dlg.ui.message.setText("帳號密碼錯誤")
            dlg.exec()
            return
        if account[0]['password'] != password:
            dlg.ui.message.setText("帳號密碼錯誤")
            dlg.exec()
            return
        dlg.ui.message.setText("登入成功")
        if dlg.exec():
            self.account = Account(db_document=account[0])
            self.accept()